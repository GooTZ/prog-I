/*
Compile: make truncate_to_n
Run: ./truncate_to_n
*/

#include "base.h"
#include "string.h"

/*
Design a function that prints and returns the length of a string, use only library functions s_length, s_sub and s_concat
*/

// string -> int
int print_and_return_length(String s);

void print_and_return_length_test() {
    check_expect_i(print_and_return_length("hallo"), 5);
}

// print and return the length of a string
int print_and_return_length(String s) {
    int string_length = s_length(s);
    printf("for string \"%s\" length is %i.\n", s, string_length);
    return string_length;
}

// string -> string
String truncate_to_n(String s, int len);

void truncate_to_n_test() {
    check_expect_s(truncate_to_n("abc", 7),"abc");
    check_expect_s(truncate_to_n("1234567", 7),"1234567");
    check_expect_s(truncate_to_n("abcdefghijk", 7),"abcdefg");

    check_expect_s(truncate_to_n("cde", 5),"cde");
    check_expect_s(truncate_to_n("hallo", 5),"hallo");
    check_expect_s(truncate_to_n("12345678", 5),"12345");
}

// Truncates the given string to a given length. Strings that are shorter are not manipulated.
String truncate_to_n(String s, int len) {
    if(s_length(s) > len) {
        return s_sub(s, 0, len);
    }
    return s;
}

int main(void) {
    print_and_return_length_test();
    truncate_to_n_test();
    return 0;
}

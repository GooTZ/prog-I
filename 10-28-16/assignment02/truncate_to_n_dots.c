/*
Compile: make truncate_to_n_dots
Run: ./truncate_to_n_dots
*/

#include "base.h"
#include "string.h"

/*
Design a function that prints and returns the length of a string, use only library functions s_length, s_sub and s_concat
*/

// string -> int
int print_and_return_length(String s);

void print_and_return_length_test() {
    check_expect_i(print_and_return_length("hallo"), 5);
}

// string -> string
String truncate_to_n_dots(String s, int len);

void truncate_to_n_dots_test() {
    check_expect_s(truncate_to_n_dots("abc", 7),"abc");
    check_expect_s(truncate_to_n_dots("1234567", 7),"1234567");
    check_expect_s(truncate_to_n_dots("abcdefghijk", 7),"abcd...");

    check_expect_s(truncate_to_n_dots("cde", 5),"cde");
    check_expect_s(truncate_to_n_dots("hallo", 5),"hallo");
    check_expect_s(truncate_to_n_dots("12345678", 5),"12...");
}

// Truncate the given string to a given length. Strings that are shorter are not manipulated.
// If the resulting length is larger or equal to 3, the last three characters are replaced by dots ('.').
String truncate_to_n_dots(String s, int len) {
    // One line is enough
    return s_length(s) > len ? (len >= 3 ? s_concat(s_sub(s, 0, len - 3), "...") : s_sub(s, 0, len)) : s;
}

// print and return the length of a string
int print_and_return_length(String s) {
    int string_length = s_length(s);
    printf("for string \"%s\" length is %i.\n", s, string_length);
    return string_length;
}

int main(void) {
    print_and_return_length_test();
    truncate_to_n_dots_test();
    return 0;
}

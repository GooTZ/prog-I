/*
Compile: make overtime
Run: ./overtime
*/

#include "base.h"

/*
Design a function that computes weekly wages with overtime from hours worked. The hourly rate is 10 €/hour. Regular working time is 40 hours/week. Overtime is paid 150% of the normal rate of pay.
*/

const int WEEKLY_HOURS = 40;

// Calculate the overtime (int -> int)
int overtime(int hours) {
    return (hours <= WEEKLY_HOURS) ? 0 : hours - WEEKLY_HOURS;
}

void overtime_test() {
    check_expect_i(overtime(0), 0);
    check_expect_i(overtime(20), 0);
    check_expect_i(overtime(39), 0);
    check_expect_i(overtime(40), 0);
    check_expect_i(overtime(41), 1);
    check_expect_i(overtime(45), 5);
}

// Compute the wage in cents given the number of hours worked.
int hours_to_wages(int hours) {
    int curr_overtime = overtime(hours);
    return (hours - curr_overtime) * 1000 + curr_overtime * 1500;
}

void hours_to_wages_test() {
    check_expect_i(hours_to_wages(0), 0);          // line 20
    check_expect_i(hours_to_wages(20), 20 * 1000); // line 21
    check_expect_i(hours_to_wages(39), 39 * 1000); // line 22
    check_expect_i(hours_to_wages(40), 40 * 1000); // line 23
    check_expect_i(hours_to_wages(41), 40 * 1000 + 1 * 1500);
    check_expect_i(hours_to_wages(45), 40 * 1000 + 5 * 1500);
}

int main(void) {
    overtime_test();
    hours_to_wages_test();
    return 0;
}

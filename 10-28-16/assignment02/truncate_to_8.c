/*
Compile: make truncate_to_8
Run: ./truncate_to_8
*/

#include "base.h"
#include "string.h"

/*
Design a function that prints and returns the length of a string, use only library functions s_length, s_sub and s_concat
*/

// string -> int
int print_and_return_length(String s);

void print_and_return_length_test() {
    check_expect_i(print_and_return_length("hallo"), 5);
}

// print and return the length of a string
int print_and_return_length(String s) {
    int string_length = s_length(s);
    printf("for string \"%s\" length is %i.\n", s, string_length);
    return string_length;
}

// string -> string
String truncate_to_8(String s);

void truncate_to_8_test() {
    check_expect_s(truncate_to_8("abc"),"abc");
    check_expect_s(truncate_to_8("12345678"),"12345678");
    check_expect_s(truncate_to_8("abcdefghijk"),"abcdefgh");
}

// Truncates the given string to a length of 8. Strings that are shorter then 8 characters are not manipulated.
String truncate_to_8(String s) {
    if(s_length(s) > 8) {
        return s_sub(s, 0, 8);
    }
    return s;
}

int main(void) {
    print_and_return_length_test();
    truncate_to_8_test();
    return 0;
}

/*
   Compile: make weights
   Run: ./weights
 */

// http://hci.uni-hannover.de/files/prog1lib/index.html
#include "base.h"
#include "string.h"
#include "arrays_lists.h"

/*
   Gegeben ist ein String-Array mit Elementen verschiedenen Gewichts. Im String-Array ist abwechselnd eine Zahl (als String) und eine Einheit (kg, g oder t) gespeichert. Ein Beispiel wäre: ["10", "kg", "500", "g", "0.5", "t"]. Ihre Aufgabe besteht darin, das Gesamtgewicht aller Elemente in Kilogramm zu berechnen. Beachten Sie dabei, dass Sie zunächst die verschiedenen Gewichte auf Kilogramm umrechnen müssen. Gewichte unbekannter Einheit können Sie ignorieren.
 */

// int[] --> int
// Returns the total weight in kg
double total_weight_kg(Array a);

static void total_weight_kg_test(void) {
    const double e = 0.0001;

    check_within_d(total_weight_kg(sa_of_string("10, kg, 500, g, 0.5, t")), 510.5, e);
    check_within_d(total_weight_kg(sa_of_string("1.35, kg, 650, g, 1.05, t, 25, Karat")), 1052.005, e);
}

double total_weight_kg(Array a) {
    double sum = 0.0;

    for(int i = 0; i < a_length(a); i += 2) {
        char* unit = get(String, a, i + 1);
        if(s_equals(unit, "t")) {
            sum += d_of_s(get(String, a, i)) * 1000.0;
        }
        else if(s_equals(unit, "kg")) {
            sum += d_of_s(get(String, a, i));
        }
        else if(s_equals(unit, "g")) {
            sum += d_of_s(get(String, a, i)) * 0.001;
        }
        else if(s_equals(unit, "Karat")) { // 2*10^-4
            sum += d_of_s(get(String, a, i)) * 0.0002;
        }
        else if(s_equals(unit, "mg")) {
            sum += d_of_s(get(String, a, i)) * 0.000001;
        }
        printf("Sum: %f\n", sum);
    }
    return sum;
}

int main(void) {
    total_weight_kg_test();
    return EXIT_SUCCESS;
}

/*
Compile: make cars
Run: ./cars
make cars && ./cars
*/

#include "base.h"
#include "arrays_lists.h"

// Car struct, includes several attributes which describe the car
typedef struct {
	String model;
	double power; // kW
	double weight; // kg
	double top_speed; // km/h
	double co2; // CO2 emissions in g/km
} Car;

/*
Constructor function for a Car.
*/
Car make_car(
	String model,
	double power, // kW
	double weight, // kg
	double top_speed, // km/h
	double co2)
{
	Car c = { model, power, weight, top_speed, co2 };
	return c;
}


/*
Array(Car) -> Array(String)
Returns the model names of the cars.
*/
Array car_models(Array cars) {
	Array models = array(String, a_length(cars));
	String model_name = "";
	for (int i = 0; i < a_length(cars); i++) {
		model_name = get(Car, cars, i).model;
		set(String, models, i, model_name);
	}
	return models;
}

/*
Array(String) -> void
Prints every String in an Array, each on a new line
*/
void print_string_array(Array strings) {
	for (int i = 0; i < a_length(strings); i++) {
		String line = get(String, strings, i);
		printf("%s\n", line);
	}
}


/*
Array(Car) -> double
Computes the sum of CO2 emissions of all cars.
*/
double average_co2_emissions(Array cars) {
	double emission_sum = 0;
	for (int i = 0; i < a_length(cars); i++) {
		emission_sum += get(Car, cars, i).co2;
	}

	return emission_sum / a_length(cars);
}


/*
Array(Car) -> Array(double)
Computes the power-to-weight ratios of the cars.
*/
Array power_weight_ratios(Array cars) {
	Array kw_per_kg = array(double, a_length(cars));
	double ratio = 0;

	for (int i = 0; i < a_length(cars); i++) {
		ratio = get(Car, cars, i).power / get(Car, cars, i).weight;
		set(double, kw_per_kg, i, ratio);
	}

	return kw_per_kg;
}

/*
Array(Car), double -> Array(Car)
Returns a new array with only those cars whose power does not exceed a limit. Returned array must not be bigger than required.
*/
Array power_less_than(Array cars, double limit) {
	int size = 0;
	for (int i = 0; i < a_length(cars); i++) {
		if (get(Car, cars, i).power < limit)
			size++;
	}

	int j = 0;
	Array newCarsArray = array(Car, size);
	for (int i = 0; i < a_length(cars); i++) {
		if (get(Car, cars, i).power < limit) {
			set(Car, newCarsArray, j, get(Car, cars, i));
		}
		j++;
	}

	return newCarsArray;
}



int main(void) {
	Array cars = array(Car, 6);
	set(Car, cars, 0, make_car("Chevrolet Spark", 38, 1070, 144, 161));
	set(Car, cars, 1, make_car("Citroen C1", 50, 805, 158, 106));
	set(Car, cars, 2, make_car("Fiat 500", 10, 499, 105, 90));
	set(Car, cars, 3, make_car("Opel Adam", 110, 1178, 210, 139));
	set(Car, cars, 4, make_car("Porsche 911 Turbo S", 427, 1675, 330, 212));
	set(Car, cars, 5, make_car("Bentley Bentayga", 447, 2422, 301, 296));
	printf("%d cars, ", a_length(cars));

	printsln("models:");
	// get models of all cars as a string array
	Array models = car_models(cars);
	// print this string array
	print_string_array(models);

	// print the average co2 emissions for all models
	printf("\nThe average CO2 emissions between all cars is %.1f g/km\n", average_co2_emissions(cars));

	// print the power by weight ratios for all models
	printsln("\npower by weight (kW/kg) ratios:");
	Array pws = power_weight_ratios(cars);
	for (int i = 0; i < a_length(pws); i++) {
		printf("%s: %.3f kW/kg\n", get(String, models, i), get(double, pws, i));
	}

	// print all models with less than LIMIT power
	double LIMIT = 100.0; // kW
	Array low_power_cars = power_less_than(cars, LIMIT);
	printf("\n%d cars have less than %.0f kW:\n", a_length(low_power_cars), LIMIT);
	for (int i = 0; i < a_length(low_power_cars); i++) {
		printsln(get(Car, low_power_cars, i).model);
	}


	return 0;
}

/*
   Compile: make closest
   Run: ./closest
 */

// http://hci.uni-hannover.de/files/prog1lib/index.html
#include "base.h"
#include "arrays_lists.h"

/*
   Gegeben sind zwei Arrays a und b mit Elementen vom Typ double. Für jedes Element von a berechnet die Funktion den Index des ähnlichsten Elements in b. Ähnlichkeit ist hier definiert als kleinste absolute Differenz. Die Funktion gibt ein Array der entsprechenden Indizes zurück.
 */

// double[], double[] --> int[]
// Returns an array with the indizies of the closest elements in array b
Array closest(Array a, Array b);

static void closest_test(void) {
    ia_check_expect(closest(da_of_string("0.1, 3.5, 2.0"), da_of_string("1.0, 2.0")),
                    ia_of_string("0, 1, 1"));

    ia_check_expect(closest(da_of_string("4.1, 13.5, 22.0"), da_of_string("5.0, 25.1, 15.5")),
                    ia_of_string("0, 2, 1"));

    ia_check_expect(closest(da_of_string("0.1, 3.5, 2.0, 1.1"), da_of_string("1.0, 2.0, 3.0, 4.0")),
                    ia_of_string("0, 2, 1, 0"));

    ia_check_expect(closest(da_of_string("1.337, 1.23, 1.2"), da_of_string("1.1, 1.2, 1.3")),
                    ia_of_string("2, 1, 1"));
}

double fabs(double x) {
    return (x >= 0) ? x : -x;
}

Array closest(Array a, Array b) {
    int length_a = a_length(a);
    int length_b = a_length(b);

    Array indizies = array(int, length_a);

    for(int i = 0; i < length_a; i++) {
        if(length_b > 0) {
            int temp = 0;
            for(int j = 1; j < length_b; j++) {
                if(fabs(get(double, a, i) - get(double, b, j)) < fabs(get(double, a, i)) - get(double, b, temp)) {
                    temp = j;
                }
            }
            set(int, indizies, i, temp);
        }
        else {
            set(int, indizies, i, -1);
        }
    }
    return indizies;
}

int main(void) {
    closest_test();
    return 0;
}

#ifndef __ARRAYS_LISTS__
#define __ARRAYS_LISTS__

#include "base.h"

/*
Internal function to output a size error.
*/
Any _size_error(int sizeof_element, int sizeof_type, String type, String file, int line) {
    printf("%s, line %d: Wrong type: Size of element is %d bytes, but size of %s is %d bytes.\n",
        file, line, sizeof_element, type, sizeof_type);
    exit(0);
    return NULL;
}

/**
Create an array of the given type and length. Examples:
- Array a = array(double, 5); // create an array of 3 doubles (initialized to 0)
- Array cars = array(Car, 3); // create an array of 4 Cars (a structure)
*/
#define array(_type, _count) \
    (a_create(_count, sizeof(_type)))

/*
Set array element at index position to value. The element type must be given 
as the first argument. Examples:
- set(double, a, 0, 3.14); // set first array element to 3.14
- set(Car, cars, 2, make_car("Fiat 500", 10, 499, 105, 90)); // set third array element
*/
#define set(_type, _array, _index, _value) { \
    if (_array->s != sizeof(_type)) { \
        _size_error(_array->s, sizeof(_type), #_type, __FILE__, __LINE__); \
    } \
    _type _t = _value; \
    a_set(_array, _index, &_t); \
}

/*
Get array element at index. The element type must be given as the first 
argument. Examples:
- double d = get(double, a, 0); // get the first array element
- Car c = get(Car, cars, 2); // get the third array element
*/
#define get(_type, _array, _index) \
    ((_array->s == sizeof(_type)) ? \
    (*(_type*)a_get(_array, _index)) : \
    (*(_type*)_size_error(_array->s, sizeof(_type), #_type, __FILE__, __LINE__)))

/*
Create an empty list for elements of the given type. Examples:
- List a = list(double); // create a list for doubles
- List cars = list(Car); // create a list for Cars (a structure)
*/
#define list(_type) \
    (l_create(sizeof(_type)))

/*
Append a new element to the end of the list. The element type must be 
given as the first argument. Examples:
- append(double, a, 1.23);
- append(Car, cars, make_car("Fiat 500", 10, 499, 105, 90));
*/
#define append(_type, _list, _value) { \
    if (_list->s != sizeof(_type)) { \
        _size_error(_list->s, sizeof(_type), #_type, __FILE__, __LINE__); \
    } \
    _type _t = _value; \
    l_append(_list, &_t); \
}    

/*
Prepend a new element to the front of the list. The element type must be 
given as the first argument. Examples:
- prepend(double, a, 1.23);
- prepend(Car, cars, make_car("Fiat 500", 10, 499, 105, 90));
*/
#define prepend(_type, _list, _value) { \
    if (_list->s != sizeof(_type)) { \
        _size_error(_list->s, sizeof(_type), #_type, __FILE__, __LINE__); \
    } \
    _type _t = _value; \
    l_prepend(_list, &_t); \
}

/*
Internal structure.
*/
typedef struct {
    List list;
    ListIterator iterator;
} Iterator;

/*
Iterators allow iterating through lists. Use the following pattern:
    Iterator iter = iterator(my_list_of_strings);
    while (has_next(iter)) {
        String value = next_value(String, iter);
        printsln(value);
    }
*/
Iterator iterator(List list) {
    Iterator i = { list, l_iterator(list) };
    return i;
}
        
/*
Test whether there is at least one more element to be iterated.
*/
bool has_next(Iterator iterator) {
    return l_has_next(iterator.iterator);
}
        
/*
Return the next value from the iterator. The element type must be 
given as the first argument. Examples:
- double d = next_value(double, iter);
- String s = next_value(String, iter);
- Car c = next_value(Car, iter);
*/
#define next_value(_type, _iterator) \
    ((_iterator.list->s == sizeof(_type)) ? \
    (*(_type*)l_next(&(_iterator.iterator))) : \
    (*(_type*)_size_error(_iterator.list->s, sizeof(_type), #_type, __FILE__, __LINE__)))

///////////////////////////////////////////////////////////////////////////
// Eample error messages:
/*

- Array a = array(2);
  set(int, a, 2, 33); // index out of range
- set(a, 1, 22); // too few arguments (type missing)
- set(int, 1, 22); // too few arguments (array missing)
- Array ds = array(double, 4);
  printiln(get(int, ds, 0)); // type error: array contains doubles
- append(int, 10); // error: too few arguments (list missing)

- list = list(Point);
- append(int, list, p); // error: wrong type

Missing arguments:
my.c:101:33: fatal error: too few arguments provided to function-like macro invocation
    set(int, i, 10);
my.c:21:9: note: macro 'set' defined here
    #define set(type, array, index, value) { \

Missing closing bracket:
my.c:153:5: fatal error: unterminated function-like macro invocation
    set(Shape, shapes, 0, make_rect(0, 0, 100, 100, black);
*/
///////////////////////////////////////////////////////////////////////////

#endif

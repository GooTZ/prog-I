/*
   Compile: make fibonacci
   Run: ./fibonacci
 */

#include "base.h"

int fibonacci_recursive(int n);

void fibonacci_recursive_test() {
    // a)
    check_expect_i(fibonacci_recursive(0), 0);
    check_expect_i(fibonacci_recursive(1), 1);
    check_expect_i(fibonacci_recursive(2), 1);
    check_expect_i(fibonacci_recursive(3), 2);
    check_expect_i(fibonacci_recursive(4), 3);
    check_expect_i(fibonacci_recursive(5), 5);
}

// calculates the nth fibonacci number
int fibonacci_recursive(int n) {
    if(n <= 0)
        return 0;
    if(n == 1)
        return 1;
    return fibonacci_recursive(n - 2) + fibonacci_recursive(n - 1);
}

int fibonacci_iterative(int n);

void fibonacci_iterative_test() {
    // a)
    check_expect_i(fibonacci_iterative(0), 0);
    check_expect_i(fibonacci_iterative(1), 1);
    check_expect_i(fibonacci_iterative(2), 1);
    check_expect_i(fibonacci_iterative(3), 2);
    check_expect_i(fibonacci_iterative(4), 3);
    check_expect_i(fibonacci_iterative(5), 5);
}

// calculates the nth fibonacci number
int fibonacci_iterative(int n) {
    if(n > 0) {
        int n2 = 0;
        int n1 = 1;
        for(int i = 1; i < n; i++) {
            int n = n2 + n1;
            n2 = n1;
            n1 = n;
        }
        return n1;
    }
    return 0;
}

int main() {
    fibonacci_recursive_test();
    fibonacci_iterative_test();

    return EXIT_SUCCESS;
}

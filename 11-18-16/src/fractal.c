/*
Compile: make fractal
Run: ./fractal
*/

// do not change these definitions and includes
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#undef assert

#include "base.h"
#include "string.h"

#undef M_PI
#define M_PI 3.1415926535897932384626433

// an Image has a defined width and height and a data array of unsigned chars, 4 for each pixel, representing its color
typedef struct {
	int width;
	int height;
	unsigned char* data;
} Image;

// a color includes red, green, blue, alpha (transparency) channels, each 0...255
typedef struct {
	unsigned char r,g,b,a;
} Color;

// a point is just a convenience struct wrapping two coordinates
typedef struct {
	int x;
	int y;
} Point;

// generate a random number between 0...1
double generateRandomNumber() {
	return ((double)rand() / (double)RAND_MAX);
}

// convenience method to generate a color
Color make_color(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	Color c = { r, g, b, a };
	return c;
}

// check whether one color equals another, excluding alpha channel
int color_equals(Color c1, Color c2) {
	return (c1.r == c2.r) && (c1.g == c2.g) && (c1.b == c2.b);
}

const Color BLACK = { 0,0,0,255 };
const Color WHITE = { 255,255,255,255 };

// return a color for a given depth of a fractal
Color getColorForDepth(int depth) {
	Color color;

	if (depth % 2) {
		color.r = 0;
		color.g = 0;
		color.b = 255;
	} else {
		color.r = 255;
		color.g = 0;
		color.b = 0;
	}

	// do not change alpha value for the assignment. You are welcome to play around with it on your own though!
	color.a = 127;

	return color;
}

// return a random color
Color getRandomColor() {
	Color color;

	color.r = (unsigned char)(generateRandomNumber() * 255);
	color.g = (unsigned char)(generateRandomNumber() * 255);
	color.b = (unsigned char)(generateRandomNumber() * 255);

	color.a = 127;

	return color;
}

// set the image completely dark (0) or completely white (255)
void clear(Image image) {
	memset(image.data, 255, image.width * image.height * 4);
}

// save the image
void saveImage(Image image, String name) {
	stbi_write_png(name, image.width, image.height, 4, image.data, image.width * 4);
}

// return the current color of a pixel in an image
Color get_pixel_color(Image image, int x, int y) {
	Color color;
	if (x >= 0 && x < image.width && y >= 0 && y < image.height) {
		color.r = image.data[y * image.width * 4 + x * 4];
		color.g = image.data[y * image.width * 4 + x * 4 + 1];
		color.b = image.data[y * image.width * 4 + x * 4 + 2];
		color.a = image.data[y * image.width * 4 + x * 4 + 3];
	} else {
		return BLACK;
	}
	return color;
}

// set the color values for a pixel in an image, taking transparency into account
void set_pixel(Image image, int x, int y, Color color) {
	if (x >= 0 && x < image.width && y >= 0 && y < image.height) {
		Color targetColor;
		targetColor.r = color.r;
		targetColor.g = color.g;
		targetColor.b = color.b;
		targetColor.a = color.a;
		// scale to 0...1
		double transparency = (double)color.a / 255.0;

		if (transparency < 1) {
			Color oldColor = get_pixel_color(image, x, y);
			if (!(color_equals(oldColor, BLACK) || color_equals(oldColor, WHITE))) {  // paint over black and white, ignoring transparency
				double newRed = ((1 - transparency) * (double)oldColor.r + transparency * (double)color.r);
				double newGreen = ((1 - transparency) * (double)oldColor.g + transparency * (double)color.g);
				double newBlue = ((1 - transparency) * (double)oldColor.b + transparency * (double)color.b);

				// scale to at least 255 over all colors
				double colorSum = newRed + newGreen + newBlue;
				double colorFactor = colorSum < 255 ? 255.0 / colorSum : 1;

				targetColor.r = (unsigned char) (newRed * colorFactor);
				targetColor.g = (unsigned char) (newGreen * colorFactor);
				targetColor.b = (unsigned char) (newBlue * colorFactor);
			}
		}

		image.data[y * image.width * 4 + x * 4] = targetColor.r;
		image.data[y * image.width * 4 + x * 4 + 1] = targetColor.g;
		image.data[y * image.width * 4 + x * 4 + 2] = targetColor.b;
		image.data[y * image.width * 4 + x * 4 + 3] = 255;
	}
}

// draw a line with two points as input
// adapted from http://rosettacode.org/wiki/Bitmap/Bresenham's_line_algorithm#C
void draw_line(Image image, int x0, int y0, int x1, int y1, Color color) {
	int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
	int dy = abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
	int err = (dx > dy ? dx : -dy) / 2, e2;

	while (true) {
		set_pixel(image, x0, y0, color);
		if (x0 == x1 && y0 == y1) break;
		e2 = err;
		if (e2 > -dx){
			err -= dy;
			x0 += sx;
		}
		if (e2 < dy){
			err += dx;
			y0 += sy;
		}
	}
}

// check whether a point p is inside the triangle between points p0,p1,p2
int pointInTriangle(Point p, Point p0, Point p1, Point p2) {
	double A = 1.0/2.0 * (-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y);
	int sign = A < 0 ? -1 : 1;
	int s = (p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y) * sign;
	int t = (p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y) * sign;

	return s > 0 && t > 0 && (s + t) < 2 * A * sign;
}

// draw a non-filled triangle with a color in an image
void draw_triangle(Image image, int x, int y, double size, Color color) {
	Point p0 = {x - size / 2, y + size / 2};
	Point p1 = {x, y - size / 2};
	Point p2 = {x + size / 2, y + size / 2};

	draw_line(image, p0.x, p0.y, p1.x, p1.y, color);
	draw_line(image, p1.x, p1.y, p2.x, p2.y, color);
	draw_line(image, p2.x, p2.y, p0.x, p0.y, color);
}

// draw a filled triangle with a color in an image
void draw_triangle_filled(Image image, int x, int y, double size, Color color) {
	Point p0 = {x - size / 2, y + size / 2};
	Point p1 = {x, y - size / 2};
	Point p2 = {x + size / 2, y + size / 2};

	for (int i = x - size; i < x + size; i++) {
		for (int j = y - size; j < y + size; j++) {
			Point p = {i,j};
			if (pointInTriangle(p, p0, p1, p2)) {
				set_pixel(image, i,j, color);
			}
		}
	}
}

// draw a non-filled circle with a color in an image
void draw_circle(Image image, int x, int y, double radius, Color color) {
	double stepSize = M_PI / (radius * 4);
	for (double delta = 0; delta < 2*M_PI; delta += stepSize) {
		set_pixel(image, x + cos(delta) * radius, y + sin(delta) * radius, color);
	}
}

// draw a filled circle with a color in an image
void draw_circle_filled(Image image, int x, int y, double radius, Color color) {
	double radiusSquared = radius*radius;
	for (int i = x - radius; i < x + radius; i++) {
		for (int j = y - radius; j < y + radius; j++) {
			double distance = (i - x)*(i - x) + (j - y)*(j - y);
			if (distance <= radiusSquared) {
				set_pixel(image, i, j, color);
			}
		}
	}
}

Image flip_image(Image image) {
	Image img_buffer;
	img_buffer.width = 1024; img_buffer.height = 1024;
	img_buffer.data = malloc(4 * img_buffer.width * img_buffer.height);  // malloc allocates bytes and returns a pointer to the first byte
	for (int x = 0; x <= image.width; x++) {
		for (int y = 0; y <= image.height; y++) {
			set_pixel(img_buffer, image.width - x, image.height - y, get_pixel_color(image, x, y));
		}
	}
	return img_buffer;
}

/*
 This function takes an image, x and y coordinates, a size (e.g. radius), a depth (recursion depth) and a maxDepth as parameters and is supposed to draw a fractal.
 Draw the fractal by recursively calling this function, increasing depth each time
 Don't forget to abort once depth > maxDepth or x or y is outside image limits
*/
void draw_fractal(Image image, int x, int y, double size, int depth, int maxDepth) {
	Color clr = getColorForDepth(depth);

	draw_circle_filled(image, x, y, size, clr);

	if (depth < maxDepth) {
		draw_fractal(image, x - (size / 2), y, size / 2, depth + 1, maxDepth);
		draw_fractal(image, x + (size / 2), y, size / 2, depth + 1, maxDepth);
	}
}

void draw_fractal_triangle(Image image, int x, int y, double size, int depth, int maxDepth) {

	if (depth < maxDepth) {
		draw_fractal_triangle(image, x - (size / 4), y, size / 2, depth + 1, maxDepth);
		draw_fractal_triangle(image, x + (size / 4), y, size / 2, depth + 1, maxDepth);
		draw_fractal_triangle(image, x, y - (size / 2), size / 2, depth + 1, maxDepth);
	}

	if (depth == maxDepth) {
		draw_triangle_filled(image, x, y, size, getRandomColor());
	}
}

void draw_fractal_randomized(Image image, int x, int y, double size, int depth, int maxDepth) {
	Color clr = getRandomColor();
	clr.a = 127;

	if (depth < maxDepth) {
		draw_fractal_randomized(image, x - (size), y, size / 2, depth + 1, maxDepth);
		draw_fractal_randomized(image, x + (size), y, size / 2, depth + 1, maxDepth);
		draw_fractal_randomized(image, x, y - (size), size / 2, depth + 1, maxDepth);
		draw_fractal_randomized(image, x, y + (size), size / 2, depth + 1, maxDepth);
	}

	draw_circle_filled(image, x, y, size, clr);
}

int main(void) {
	// init random number generator with time as seed
	srand(time(NULL));

	// init image struct
	Image image;
	image.width = 1024;
	image.height = 1024;
	image.data = malloc(4 * image.width * image.height);  // malloc allocates bytes and returns a pointer to the first byte

	clear(image);
	printf("drawing the fractal\n");
	draw_fractal(image, image.width / 2, image.height / 2, image.width / 2 - 10, 0, 4);
	saveImage(image, "fractal_circle.png");

	clear(image);
	printf("drawing the fractal\n");
	draw_fractal_triangle(image, image.width / 2, image.height - 20, image.width - 10, 0, 6);
	// Flip the image upside down

	saveImage(flip_image(image), "fractal_triangle.png");

	clear(image);
	printf("drawing the fractal\n");
	draw_fractal_randomized(image, image.width / 2, image.height / 2, image.width / 4 - 10, 0, 12);
	saveImage(image, "fractal_random.png");

	printf("Done!\n");
}

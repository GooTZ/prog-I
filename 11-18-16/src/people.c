/*
   Compile: make people
   Run: ./people
 */

#include "base.h"
#include "string.h"
#include "people.h"

// b) ... make_statistics...
Statistics make_statistics(int mean_year, int number_m, int number_f, float mean_height_m, float mean_height_f) {
    Statistics s = { mean_year, number_m, number_f, mean_height_m, mean_height_f };
    return s;
}

// c) ... print_statistics...
void print_statistics(Statistics s) {
    printf("{\n\tmean_year: %4i;\n\tnumber_m: %i;\n\tnumber_f: %i;\n\tmean_height_m: %2.2f;\n\tmean_height_f: %2.2f;\n}\n",
           s.mean_year, s.number_m, s.number_f, s.mean_height_m, s.mean_height_f);
}

// d) ... compute_statistics...
Statistics compute_statistics(String file) {
    Statistics s = make_statistics(0, 0, 0, 0.0, 0.0);
    const int length = s_length(file);
    int line_start = s_index(file, ROW_PREFIX) + 1;

    int year_sum = 0;
    double height_m_sum = 0.0;
    double height_f_sum = 0.0;

    while(line_start < length) {
        int row_end = s_index_from(file, ROW_PREFIX, line_start);

        int col_0_end = s_index_from(file, COLUMN_PREFIX, line_start);
        int col_1_end = s_index_from(file, COLUMN_PREFIX, col_0_end + 1);

        int year = i_of_s(s_sub(file, line_start, col_0_end));
        char sex = file[col_0_end + 1];
        double height = d_of_s(s_sub(file, col_1_end + 1, row_end));
        // printf("%i\t%c\t%.2f\n", year, sex, height);

        year_sum += year;

        if(sex == MALE_CHAR) {
            s.number_m++;
            height_m_sum += height;
        } else {
            s.number_f++;
            height_f_sum += height;
        }

        line_start = row_end + 1;
    }
    s.mean_year = year_sum / (s.number_m + s.number_f);
    s.mean_height_m = height_m_sum / ((double)s.number_m);
    s.mean_height_f = height_f_sum / ((double)s.number_f);
    return s;
}

int main() {
    String table = s_read_file("people.txt");
    // printsln(table); // spam

    Statistics s = compute_statistics(table);
    print_statistics(s);
    return EXIT_SUCCESS;
}

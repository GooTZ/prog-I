// e) header file...
#ifndef __PEOPLE_H__
#define __PEOPLE_H__

const String COLUMN_PREFIX = "\t";
const String ROW_PREFIX = "\n";
const char MALE_CHAR = 'm';

// a) struct Statistics ...
typedef struct {
    int mean_year;
    int number_m;
    int number_f;
    float mean_height_m;
    float mean_height_f;
} Statistics;

// b) ... make_statistics...
Statistics make_statistics(int mean_year, int number_m, int number_f, float mean_height_m, float mean_height_f);

// c) ... print_statistics...
void print_statistics(Statistics s);

// d) ... compute_statistics...
Statistics compute_statistics(String file);

#endif

/*
Compile: make sum_even_cubes
Run: ./sum_even_cubes
*/

#include "base.h"

int sum_even_cubes(int n);

void sum_even_cubes_test(void)
{
	printf("Checks sum_even_cubes\n");
	check_expect_i(sum_even_cubes(1), 0);
	check_expect_i(sum_even_cubes(2), 8);
	check_expect_i(sum_even_cubes(3), 8);
	check_expect_i(sum_even_cubes(4), 72);
	check_expect_i(sum_even_cubes(5), 72);
}

int sum_even_cubes(int n)
{
	int sum = 0;
	for (int i = 0; i <= n; i++) {
		if (!(i % 2)) {
			sum += pow(i, 3);
		}
	}
	return sum;
}

int sum_even_cubes_rec(int n);

int sum_even_cubes_rec(int n)
{
	int sum = 0;

	if (!(n % 2) && n) {
		sum += pow(n, 3);
	}

	if (n) {
		sum += sum_even_cubes_rec(n - 1);
	}

	return sum;
}

void sum_even_cubes_rec_test(void)
{
	printf("Checks sum_even_cubes_rec\n");
	check_expect_i(sum_even_cubes_rec(1), 0);
	check_expect_i(sum_even_cubes_rec(2), 8);
	check_expect_i(sum_even_cubes_rec(3), 8);
	check_expect_i(sum_even_cubes_rec(4), 72);
	check_expect_i(sum_even_cubes_rec(5), 72);
}

int sum_even_cubes_rec2(int n, int tmp);

void sum_even_cubes_rec2_test(void)
{
	printf("Checks sum_even_cubes_rec\n");
	check_expect_i(sum_even_cubes_rec2(1, 0), 0);
	check_expect_i(sum_even_cubes_rec2(2, 0), 8);
	check_expect_i(sum_even_cubes_rec2(3, 0), 8);
	check_expect_i(sum_even_cubes_rec2(4, 0), 72);
	check_expect_i(sum_even_cubes_rec2(5, 0), 72);
}

int sum_even_cubes_rec2(int n, int tmp)
{
	if (!(n % 2)) {
		tmp += pow(n, 3);
	}

	if (n) {
		tmp = sum_even_cubes_rec2(n - 1, tmp);
	}

	return tmp;
}


int main(void)
{
	sum_even_cubes_test();
	sum_even_cubes_rec_test();
	sum_even_cubes_rec2_test();
}

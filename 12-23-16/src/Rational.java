import prog1.base.Base;

/*
    Windows:
    Compile: javac -cp .;prog1javalib.jar Rational.java
    Run: java -cp .;prog1javalib.jar Rational

    OS X:
    Compile: javac -cp .:prog1javalib.jar Rational.java
    Run: java -cp .:prog1javalib.jar Rational
 */

public class Rational {

    int numerator, denominator;

    public Rational(int numerator, int denominator) {
        if (denominator == 0) {
            throw new RuntimeException("zero denominator");
        }
        int d = greatestCommonDivisor(numerator, denominator);
        if(denominator < 0)
            d = -d;
        this.numerator = numerator / d;
        this.denominator = denominator / d;
    }

    public static Rational r(int numerator, int denominator) {
        return new Rational(numerator, denominator);
    }

    @Override
    public String toString() {
        return numerator + (denominator != 1 && numerator != 0
                            ? ("/" + denominator) : "");
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof Integer) {
                return denominator == 1 && numerator == (int) o
                        || numerator == 0 && (int) o == 0;
            }
            if (o instanceof Rational) {
                Rational r = (Rational) o;
                return numerator == r.numerator && denominator == r.denominator;
            }
        }
        return false;
    }

    public Rational plus(Rational r) {
        return r(numerator * r.denominator + r.numerator * denominator,
                 denominator * r.denominator);
    }

    public Rational minus(Rational r) {
        return r(numerator * r.denominator - r.numerator * denominator,
                 denominator * r.denominator);
    }

    public Rational times(Rational r) {
        return r(numerator * r.numerator, denominator * r.denominator);
    }

    public Rational dividedBy(Rational r) {
        return r(numerator * r.denominator, denominator * r.numerator);
    }

    public Rational inverse() {
        return r(denominator, numerator);
    }

    public Rational plus(int i) {
        return plus(r(i, 1));
    }

    public Rational minus(int i) {
        return minus(r(i, 1));
    }

    public Rational times(int i) {
        return times(r(i, 1));
    }

    public Rational dividedBy(int i) {
        return dividedBy(r(i, 1));
    }

    private static int greatestCommonDivisor(int x, int y) {
        int t = 1;
        // Abs
        int x1 = x >= 0 ? x : -x;
        int y1 = y >= 0 ? y : -y;
        for (int i = x1 < y1 ? x1 : y1; i > 1 && t == 1; i--) {
            if(x % i == 0 && y % i == 0) {
                t = i;
            }
        }
        return t;
    }

    public static void rationalTest() {
        // 6/8 --> 3/4
        Base.checkExpect(r(6, 8), r(3, 4));
        Base.checkExpect(r(6, 8).toString(), "3/4");

        // 6/-8 --> -3/4
        Base.checkExpect(r(6, -8), r(3, -4));
        Base.checkExpect(r(6, -8).toString(), "-3/4");

        // 1/-2 --> -1/2
        Base.checkExpect(r(1,-2), r(-1, 2));
        Base.checkExpect(r(1,-2).toString(), "-1/2");

        // 1/2 + 3/4 = 5/4
        Base.checkExpect(r(1, 2).plus(r(3, 4)), r(5, 4));
        Base.checkExpect(r(1, 2).plus(r(3, 4)).toString(), "5/4");

        // 1/2 - 3/4 = -1/4
        Base.checkExpect(r(1, 2).minus(r(3, 4)), r(-1, 4));
        Base.checkExpect(r(1, 2).minus(r(3, 4)).toString(), "-1/4");

        // 1/3 + 2/3 = 1
        Base.checkExpect(r(1, 3).plus(r(2, 3)), 1);
        Base.checkExpect(r(1, 3).plus(r(2, 3)).toString(), "1");

        // 1/3 - 1/3 = 0
        Base.checkExpect(r(1, 3).minus(r(1, 3)), 0);
        Base.checkExpect(r(1, 3).minus(r(1, 3)).toString(), "0");

        // 1/3 - 4/3 = -1
        Base.checkExpect(r(1, 3).minus(r(4, 3)), -1);
        Base.checkExpect(r(1, 3).minus(r(4, 3)).toString(), "-1");

        // 1/2 * 4/3 = 2/3
        Base.checkExpect(r(1, 2).times(r(4, 3)), r(2, 3));

        // -1/2 * 4/3 = -2/3
        Base.checkExpect(r(-1, 2).times(r(4, 3)), r(-2, 3));

        // (1/2) / (4/3) = 3/8
        Base.checkExpect(r(1, 2).dividedBy(r(4, 3)), r(3, 8));

        // (-1/2) / (4/3) = -3/8
        Base.checkExpect(r(-1, 2).dividedBy(r(4, 3)), r(-3, 8));

    }

    public static void main(String[] args) {
        rationalTest();
        Base.summary();
    }

}


import java.util.Arrays;

public class BookShop {

	public static void main(String[] args) {
		BookShop bs = new BookShop(10);
		bs.addBook("Erebos", "9783462046328", 19.99);
		bs.addBook("Eragon", "9789255374826", 14.99);
		bs.addBook("Herr der Ringe", "9782374927496", 24.99);
		bs.addBook("Silo", "9786385082957", 9.99);
		bs.addBook("Erebos", "9783462046328", 19.99);

		bs.printBooks();
		System.out.println("BookShop.getBook() "+bs.getBook("Silo")+"\n");

		System.out.println("Search book:");
		System.out.println("Result: "+bs.getBook(System.console().readLine()));
	}

	private Book[] books;
	private int count = 0;

	public BookShop(int maxCount) {
		books = new Book[maxCount];
	}

	public void addBook(Book value) {
		if(getBook(value.getTitle()) == null)
			if(count < books.length)
				books[count++] = value;
	}

	public void addBook(String title, String isbn, double price) {
		addBook(new Book(title, isbn, price));
	}

	public void addBooks(Book... books) {
		for (Book book : books) {
			addBook(book);
		}
	}

	public Book getBook(String title) {
		for (int i = 0; i < count; i++) {
			if(books[i].getTitle().equals(title))
				return books[i];
		}
		return null;
	}

	public void printBooks() {
		String out = "Books:\n";
		for (int i = 0; i < count; i++) {
			out += books[i] + "\n";
		}
		System.out.print(out);
	}

	@Override
	public String toString() {
		return "Books (" + Arrays.toString(books) + ")";
	}
}

/*
Windows:
Compile: javac -cp .;prog1javalib.jar MergeSort.java
Run: java -cp .;prog1javalib.jar MergeSort

OS X:
Compile: javac -cp .:prog1javalib.jar MergeSort.java
Run: java -cp .:prog1javalib.jar MergeSort
*/

import prog1.base.Base;
import java.util.Arrays;

public class MergeSort {

	private int[] inputArray;
	private int[] tempValuesArray;

	public int[] sort(int[] inputArray) {
        if (inputArray == null) return null;
		this.inputArray = inputArray;
		this.tempValuesArray = new int[inputArray.length];
		doMergeSort(0, inputArray.length - 1);
        return this.inputArray;
	}

	private void doMergeSort(int lowerIndex, int higherIndex) {
		if(lowerIndex < higherIndex) {
			int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
			doMergeSort(lowerIndex, middle);
			doMergeSort(middle + 1, higherIndex);
			mergePartitions(lowerIndex, middle, higherIndex);
		}

	}

	private void mergePartitions(int lowerIndex, int middle, int higherIndex) {
        // set elements in the temp array
		for (int i = lowerIndex; i <= higherIndex; i++) {
			tempValuesArray[i] = inputArray[i];
		}

		// todo: implement merge partitions. This can be a bit tricky but is simply a logic puzzle. Use the tempValuesArray, compare values there and order them correctly when putting them into the inputArray. A partition may be more than one value...
		int index1 = lowerIndex;
		int index2 = middle + 1;
		for (int i = lowerIndex; i <= higherIndex; i++) {
			if(index1 > middle) {
				inputArray[i] = tempValuesArray[index2];
				index2++;
			} else if (index2 > higherIndex) {
				inputArray[i] = tempValuesArray[index1];
				index1++;
			} else if(tempValuesArray[index1] < tempValuesArray[index2]) {
				inputArray[i] = tempValuesArray[index1];
				index1++;
			} else {
				inputArray[i] = tempValuesArray[index2];
				index2++;
			}
		}
		// System.out.println(Arrays.toString(inputArray));
	}


    public static void main(String[] args){

		MergeSort ms = new MergeSort();

        int test1[] = {3,1,2};
        Base.checkExpect(ms.sort(test1), new int[] {1,2,3});

		int []test2 = {38,27,43,3,9,82,10};
        Base.checkExpect(ms.sort(test2), new int[] {3,9,10,27,38,43,82});

		int[] test3 = {38,-27,3,9,82,10};
        Base.checkExpect(ms.sort(test3), new int[] {-27,3,9,10,38,82});

	}
}

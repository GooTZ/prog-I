# static predefined targets (uncomfortable)
TARGETS = $(shell find * -maxdepth 1 -type d)
LATEXFILE = solution.tex

# clear the targets
.PHONY: clean all $(TARGETS)

# Make the given taget ($@)
$(TARGETS):
	if [ -f "$@/$(LATEXFILE)" ]; then latex -interaction=nonstopmode -output-format=pdf -output-directory=$@ $@/$(LATEXFILE); fi
	$(MAKE) -C $@

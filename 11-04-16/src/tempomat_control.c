/*
Compile: make tempomat_control
Run: ./tempomat_control
*/

#include "base.h"

/*
Implement a tempomat that keeps a certain speed within limits. The tempomat shall accelerate if the current speed is 2% or more less than the target, do nothing while the speed is within +- 2% of the intended target and brake of the current speed is more than 2% off the intended target. If the intended target is less that 0, an emergency stop (hard stop) is intended.
*/


const double V_STAND = 0.0;
const double V_INPUT_HIGH = 300.0;


typedef enum OperatingMode {
	S_EMERGENCY_BRAKE,
	S_IDLE,
	S_BRAKE,
	S_ACCELERATE
} OperatingMode;

// double -> OperatingMode
// Return the operating mode of the speed control unit given the speed.
OperatingMode speed_control(double current_speed, double target_speed);

static void speed_control_test() {
	// TODO: rework test cases
	check_expect_i(speed_control(301, 50), S_IDLE);
	check_expect_i(speed_control(50, 60), S_ACCELERATE);
	check_expect_i(speed_control(50, 40), S_BRAKE);
	check_expect_i(speed_control(50, -1), S_EMERGENCY_BRAKE);
	check_expect_i(speed_control(100, 101), S_IDLE);
	check_expect_i(speed_control(-1,100), S_IDLE);
}

// Return the operating mode of the climate control unit given the speed.
OperatingMode speed_control(double current_speed, double target_speed) {
	if (target_speed < V_STAND)
		return S_EMERGENCY_BRAKE;
	if (current_speed < V_STAND || current_speed > V_INPUT_HIGH)
		return S_IDLE;
	if (current_speed > (target_speed / 100) * 102)
		return S_BRAKE;
	if (current_speed < (target_speed / 100) * 98)
		return S_ACCELERATE;
	if (current_speed < ((target_speed / 100) * 102) || current_speed > (target_speed / 100) * 98)
		return S_IDLE;
	return S_IDLE;
}

int main(void) {
	speed_control_test();
	return 0;
}

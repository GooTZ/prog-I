/*
   Compile: make sail
   Run: ./sail
 */

#include "base.h"

/*
   In einer Spielewelt sei die Erde eine Scheibe mit den Kontinenten Europa, Afrika
   und Amerika. Afrika liegt südlich von Europa. Amerika liegt westlich von Europa
   und Afrika. In der Spielewelt können Nord-, Süd-, Ost- und Westwinde auftreten.
   In der Spielewelt gibt es genau ein Segelschiff. Wenn das Segelschiff bei
   Westwind in Europa startet, fährt es nach Amerika. Wenn es bei Ostwind in
   Amerika startet, landet es (auf Grund einer nördlichen Wasserströmung) in Europa.
   Wenn es bei Westwind von Amerika aus startet, fällt es ins Nichts. Wenn es bei
   Südwind in Europa startet, landet es in Afrika. Entsprechendes gilt für die a
   nderen möglichen Kombinationen von Startposition und Windrichtung. Nehmen Sie an,
   dass sich die Windrichtung nur vor, aber nie während einer Reise des
   Segelschiffs ändert. Entwickeln Sie eine Funktion, die abhängig von der
   Startposition des Segelschiffs und der Windrichtung beim Start die Zielposition
   bestimmt.

   Europa + Westwind -> Amerika  <>  Amerika + Ostwind -> Europa
   Amerika + Westwind -> Nichts
   Europa + Südwind -> Afrika  <> Afrika + Nordwind -> Europa

                        Nichts
             Nichts       ^
               ^    /< Europa > Nichts
   Nichts < Amerika >/    v
               v    \     ^
             Nichts  \< Afrika > Nichts
                          v
                        Nichts
 */

enum Continents {
	EUROPE,
	AMERICA,
	AFRICA,
	NONE
};

enum Winds {
	NORTH,
	SOUTH,
	EAST,
	WEST
};

// enum Continents, enum Winds -> enum Continents
enum Continents sail(enum Continents curr, enum Winds wind);

static void sail_test() {
	check_expect_i(sail(EUROPE, WEST), AMERICA);
	check_expect_i(sail(AMERICA, WEST), NONE);
	check_expect_i(sail(EUROPE, SOUTH), AFRICA);
}

// Return the next continent where the ship arrives due to the given start positon and wind direction
enum Continents sail(enum Continents curr, enum Winds wind) {
	switch(curr) {
	case EUROPE:
		if(wind == WEST) {
			return AMERICA;
		}
		if(wind == SOUTH) {
			return AFRICA;
		}
		break;

	case AMERICA:
		if(wind == EAST) {
			return EUROPE;
		}
		break;

	case AFRICA:
		if(wind == NORTH) {
			return EUROPE;
		}
		if(wind == WEST) {
			return AMERICA;
		}
		break;

	case NONE:
		break;
	}
	return NONE;
}

int main(void) {
	sail_test();
	return EXIT_SUCCESS;     // constants... (DRY)
}

package postfix;

import java.util.Random;
import prog1.base.Base;

/**
 * PostFix language objects like integer values, plus operator, print-stack command, etc.
 * @author michaelrohs
 */
public abstract class Obj {

    /**
     * Execute the object. This can have different semantics, depending on the
     * concrete type of the object. For integers execution means pushing onto
     * the stack. For the + operator it means taking two elements from the stack,
     * adding them, and pushing the result back on the stack. In almost all
     * cases execution consumes its stack operands.
     *
     * Each implementation should document the stack effect, e.g.,
     * a b --> a+b
     * means b is at the top of the stack, the operator takes a and b from the
     * stack, adds them, and puts the result back on the stack.
     *
     * @param stack the operand stack
     */
    public abstract void execute(OperandStack stack);

    /**
     * Convert an object to an int. Only works for Int objects.
     * @return integer value
     */
    public int toInt() {
        if (!(this instanceof Int)) {
            throw new RuntimeException("Expecting Int, but found " + getClass().getSimpleName() + ": " + toString());
        }
        Int i = (Int) this;
        return i.value;
    }

    /**
     * Convert an object to an Array. Only works for Array objects.
     * @return integer value
     */
    public Array toArray() {
        if (!(this instanceof Array)) {
            throw new RuntimeException("Expecting Array, but found " + getClass().getSimpleName() + ": " + toString());
        }
        Array a = (Array) this;
        return a;
    }

    /**
     * Print the object type in a nice way.
     * @return the object class name
     */
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    /**
     * The number of elements of the object (may differ from 1 for Strs and Arrays).
     * @return the number of elements
     */
    public int length() {
        return 1;
    }

}

class Int extends Obj {
    public final int value;

    public Int(int value) {
        this.value = value;
    }

    @Override
    public void execute(OperandStack stack) {
        stack.push(this);
    }

    @Override
    public String toString() {
        return "" + value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (getClass() != o.getClass()) return false;
        Int i = (Int) o;
        return value == i.value;
    }

    @Override
    public int hashCode() {
        return value;
    }

}

class Plus extends Obj {

    @Override
    public void execute(OperandStack stack) { // i j --> i+j
        int j = stack.pop().toInt();
        int i = stack.pop().toInt();
        stack.push(new Int(i + j));
    }

    @Override
    public String toString() {
        return "+";
    }

}

class Minus extends Obj {

    @Override
    public void execute(OperandStack stack) { // i j --> i-j
        int j = stack.pop().toInt();
        int i = stack.pop().toInt();
        stack.push(new Int(i - j));
    }

    @Override
    public String toString() {
        return "-";
    }

}

class Mul extends Obj {

    @Override
    public void execute(OperandStack stack) { // i j --> i*j
        int j = stack.pop().toInt();
        int i = stack.pop().toInt();
        stack.push(new Int(i * j));
    }

    @Override
    public String toString() {
        return "*";
    }

}

class Div extends Obj {

    @Override
    public void execute(OperandStack stack) { // i j --> i/j
        int j = stack.pop().toInt();
        int i = stack.pop().toInt();
        stack.push(new Int(i / j));
    }

    @Override
    public String toString() {
        return "/";
    }

}

class LT extends Obj { // less than (<)

    @Override
    public void execute(OperandStack stack) { // a b
        int b = stack.pop().toInt();
        int a = stack.pop().toInt();
        stack.push(new Int(a < b ? 1 : 0));
    }

    @Override
    public String toString() {
        return "<";
    }

}

class GT extends Obj { // greater than (>)

    @Override
    public void execute(OperandStack stack) { // a b
        int b = stack.pop().toInt();
        int a = stack.pop().toInt();
        stack.push(new Int(a > b ? 1 : 0));
    }

    @Override
    public String toString() {
        return ">";
    }

}

class EQ extends Obj { // equal to (=)

    @Override
    public void execute(OperandStack stack) { // a b
        Obj b = stack.pop();
        Obj a = stack.pop();
        stack.push(new Int(a.equals(b) ? 1 : 0));
    }

    @Override
    public String toString() {
        return "=";
    }

}

class And extends Obj {

    @Override
    public void execute(OperandStack stack) { // a b
        int b = stack.pop().toInt();
        int a = stack.pop().toInt();
        stack.push(new Int((a != 0) && (b != 0) ? 1 : 0));
    }

    @Override
    public String toString() {
        return "and";
    }

}

class Or extends Obj {

    @Override
    public void execute(OperandStack stack) { // a b
        int b = stack.pop().toInt();
        int a = stack.pop().toInt();
        stack.push(new Int((a != 0) || (b != 0) ? 1 : 0));
    }

    @Override
    public String toString() {
        return "or";
    }

}

class Not extends Obj {

    @Override
    public void execute(OperandStack stack) { // a
        int a = stack.pop().toInt();
        stack.push(new Int((a == 0) ? 1 : 0));
    }

    @Override
    public String toString() {
        return "not";
    }

}

class PrintStack extends Obj {

    @Override
    public void execute(OperandStack stack) {
        Base.println(stack.toString());
    }

    @Override
    public String toString() {
        return ".s";
    }

}

class PrintTop extends Obj {

    @Override
    public void execute(OperandStack stack) { // a
        Base.println(stack.pop());
    }

    @Override
    public String toString() {
        return ".";
    }

}

class Str extends Obj {

    public final String value;

    public Str(String value) {
        this.value = value;
    }

    @Override
    public void execute(OperandStack stack) {
        stack.push(this);
    }

    @Override
    public int length() {
        return value.length();
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (getClass() != o.getClass()) return false;
        Str s = (Str) o;
        if (value == null && s.value == null) return true;
        if (value == null || s.value == null) return false;
        return value.equals(s.value);
    }

    @Override
    public int hashCode() {
        if (value == null) return 0;
        return value.hashCode();
    }

}

class Length extends Obj {

    @Override
    public void execute(OperandStack stack) { // a --> n
        Obj a = stack.pop();
        stack.push(new Int(a.length()));
    }

    @Override
    public String toString() {
        return "length";
    }

}

class Get extends Obj {

    @Override
    public void execute(OperandStack stack) { // a i --> a[i]
        int i = stack.pop().toInt();
        Obj o = stack.pop();
        if (o instanceof Str) {
            Str s = (Str) o;
            stack.push(new Int(s.value.charAt(i)));
        } else if (o instanceof Array) {
            Array a = (Array) o;
            stack.push(a.values[i]);
        }
    }

    @Override
    public String toString() {
        return "get";
    }

}

class Set extends Obj {

    @Override
    public void execute(OperandStack stack) { // a i v --> a'
        Obj v = stack.pop();
        int i = stack.pop().toInt();
        Obj o = stack.pop();
        if (o instanceof Str && v instanceof Int) {
            Str s = (Str) o;
            byte[] bytes = s.value.getBytes();
            bytes[i] = (byte) v.toInt();
            s = new Str(new String(bytes));
            stack.push(s);
        } else if (o instanceof Array) {
            Array a = (Array) o;
            a.values[i] = v;
            stack.push(a);
        }
    }

    @Override
    public String toString() {
        return "set";
    }

}

class Rand extends Obj {

    private static final Random rnd = new Random(System.currentTimeMillis());

    @Override
    public void execute(OperandStack stack) { // n
        int n = stack.pop().toInt();
        stack.push(new Int(rnd.nextInt(n)));
    }

}

class Pop extends Obj {
    @Override
    public void execute(OperandStack stack) { // a -->
        stack.pop();
    }
}

class Swap extends Obj {
    @Override
    public void execute(OperandStack stack) { // a b --> b a
        Obj b = stack.pop();
        Obj a = stack.pop();
        stack.push(b);
        stack.push(a);
    }
}

class Dup extends Obj {
    @Override
    public void execute(OperandStack stack) { // a --> a a
        Obj a = stack.pop();
        stack.push(a);
        stack.push(a);
    }
}

class Copy extends Obj {
    @Override
    public void execute(OperandStack stack) { // an ... a0 i --> an ... a0 ai
        int i = stack.pop().toInt();
        Obj o = stack.get(stack.count() - 1 - i);
        stack.push(o);
    }
}

class Clear extends Obj {
    @Override
    public void execute(OperandStack stack) { // ... -->
        stack.clear();
    }
}

class ReadInt extends Obj {
    @Override
    public void execute(OperandStack stack) { // --> i
        stack.push(new Int(Base.intInput()));
    }
}

class ReadChar extends Obj {
    @Override
    public void execute(OperandStack stack) { // --> c
        try {
            stack.push(new Int(System.in.read() & 0xff));
        } catch (Exception ex) {
            throw new RuntimeException(ex.toString());
        }
    }
}

class ArrayOpen extends Obj {
    @Override
    public void execute(OperandStack stack) { // --> {
        stack.push(this);
    }

    @Override
    public String toString() {
        return "{";
    }
}

class ArrayClose extends Obj {
    @Override
    public void execute(OperandStack stack) { // { ... --> { ... } (new array)
        int arrayOpenIndex = stack.count() - 1;
        while (arrayOpenIndex >= 0 && !(stack.get(arrayOpenIndex) instanceof ArrayOpen)) {
            arrayOpenIndex--;
        }
        if (arrayOpenIndex < 0) {
            throw new RuntimeException("Array open not found.");
        }
        int n = stack.count() - arrayOpenIndex - 1;
        Array a = new Array(n);
        for (int i = n - 1; i >= 0; i--) {
            a.values[i] = stack.pop();
        }
        stack.pop(); // pop open bracket from stack
        stack.push(a); // push new finished array onto stack
    }
}

class Array extends Obj {

    public final Obj[] values;

    public Array(int n) {
        values = new Obj[n];
    }

    @Override
    public void execute(OperandStack stack) {
        stack.push(this);
    }

    public void executeElements(OperandStack stack) {
        for (Obj o : values) {
            if (PostFix.DEBUG_LEVEL >= 2) Base.println("Executing (in executable array): " + o);
            o.execute(stack);
            if (PostFix.DEBUG_LEVEL >= 1) Base.println(stack.toString());
            if (PostFix.DEBUG_LEVEL >= 3) Base.stringInput(); // wait for return key
        }
    }

    @Override
    public int length() {
        return values.length;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        for (int i = 0; i < values.length; i++) {
            sb.append(values[i].toString());
            if (i < values.length - 1) {
                sb.append(" ");
            }
        }
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (getClass() != o.getClass()) return false;
        Array a = (Array) o;
        int n = values.length;
        if (n != a.values.length) return false;
        // does not check for cycles!
        for (int i = 0; i < n; i++) {
            if (!values[i].equals(a.values[i])) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 31 * values.length;
        for (Obj v : values) {
            if (v != null) {
                hash += 31 * v.hashCode();
            }
        }
        return hash;
    }

}

class If extends Obj {
    @Override
    public void execute(OperandStack stack) { // cond then else
        Array elsePart = stack.pop().toArray();
        Array thenPart = stack.pop().toArray();
        int cond = stack.pop().toInt();
        if (cond != 0) {
            thenPart.executeElements(stack);
        } else {
            elsePart.executeElements(stack);
        }
    }
}

class Cond extends Obj {
    @Override
    public void execute(OperandStack stack) { // array of condition-action pairs
        Array pairs = stack.pop().toArray();
        int n = pairs.values.length;
        if ((n % 2) == 1) {
            throw new RuntimeException("Need an even number of elements in condition-action array.");
        }
        for (int i = 0; i < n; ) {
            Array condition = pairs.values[i++].toArray();
            Array action = pairs.values[i++].toArray();
            condition.executeElements(stack);
            int result = stack.pop().toInt();
            if (result != 0) {
                action.executeElements(stack);
                break; // exit loop, do not consider further conditions
            }
        }
    }
}

class While extends Obj {
    @Override
    public void execute(OperandStack stack) { // cond proc
        Array proc = stack.pop().toArray();
        int cond = stack.pop().toInt();
        while (cond != 0) {
            if (PostFix.DEBUG_LEVEL >= 2) Base.println("Executing (in while): " + proc);
            proc.executeElements(stack);
            cond = stack.pop().toInt(); // for next iteration (if any)
            if (PostFix.DEBUG_LEVEL >= 1) Base.println(stack.toString());
            if (PostFix.DEBUG_LEVEL >= 3) Base.stringInput(); // wait for return key
        }
    }
}

class Run extends Obj {

    private final PostFix pf;

    public Run(PostFix pf) {
        this.pf = pf;
    }

    @Override
    public void execute(OperandStack stack) { // 'filename' (execute file)
        String filename = ((Str)stack.pop()).value;
        String sourceCode = Base.sReadFile(filename);
        pf.interpret(sourceCode);
    }

}

// Enters the definition: name -> obj
class Def extends Obj {

    private final String name;
    private final NamedObjects definedObjects;

    public Def(String name, NamedObjects definedObjects) {
        if (PostFix.DEBUG_LEVEL >= 2) Base.println("Def constructor: " + name);
        this.name = name;
        this.definedObjects = definedObjects;
    }

    @Override
    public void execute(OperandStack stack) { // obj
        Obj obj = stack.pop();
        if (PostFix.DEBUG_LEVEL >= 2) Base.println("Def execute: " + name + ", " + obj);
        definedObjects.put(name, obj);
    }

    @Override
    public String toString() {
        return name + '!';
    }

}

// Uses the definition: name -> obj
class Ref extends Obj {

    private final String name;
    private final NamedObjects definedObjects;

    public Ref(String name, NamedObjects definedObjects) {
        if (PostFix.DEBUG_LEVEL >= 2) Base.println("Ref constructor: " + name);
        this.name = name;
        this.definedObjects = definedObjects;
    }

    @Override
    public void execute(OperandStack stack) {
        if (PostFix.DEBUG_LEVEL >= 2) Base.println("Ref execute: " + name);
        Obj value = definedObjects.get(name);
        if (value == null) {
            throw new RuntimeException("Undefined reference: " + name);
        } else {
            if (value instanceof Array) {
                // for arrays, execute pushes the array on the stack
                // but when accessed through a reference,
                // then execute the elements of the array
                ((Array)value).executeElements(stack);
            } else {
                value.execute(stack);
            }
        }
    }

    @Override
    public String toString() {
        return name;
    }

}

class BuildString extends Obj {
	@Override
	public void execute(OperandStack stack) {
		Obj s1 = stack.pop();
		Obj s2 = stack.pop();

		if (s1 instanceof Str && s2 instanceof Str) {
			String elTexto = ((Str)s2).value + ((Str)s1).value;
			stack.push(new Str(elTexto));
		} else {
			throw new RuntimeException("Not enough Strings on the Stack!");
		}
	}

	@Override
	public String toString() {
		return "buildString";
	}
}

class SumCheck extends Obj {
	@Override
	public void execute(OperandStack stack) {
		Obj base = stack.pop();

		if (!(base instanceof Int)) {
			throw new RuntimeException("Can not find Integer on the Stack!");
		}

		Integer baseInt = ((Int)base).value;
		Integer result = 0;

		Obj next = new Int(0);
		while(stack.count() > 0) {
			next = stack.pop();
			if (next instanceof Int) {
				result += ((Int)next).value;
			} else {
				stack.push(next);
				break;
			}
		}

		if (result == baseInt) {
			stack.push(new Int(1));
		} else {
			stack.push(new Int(0));
		}

	}

	@Override
	public String toString() {
		return "sumCheck";
	}
}

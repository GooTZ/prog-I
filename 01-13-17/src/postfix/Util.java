package postfix;

/**
 * Some static helper functions.
 * @author michaelrohs
 */
public class Util {

    /**
     * Check whether s is a string representation of an integer number.
     * @param s
     * @return true if integer, false otherwise
     */
    public static boolean isInteger(String s) {
        int n = s.length();
        if (n <= 0) return false;
        char c = s.charAt(0);
        if (n == 1) return Character.isDigit(c);
        // assert: n >= 2
        if (!(c == '-' || c == '+' || Character.isDigit(c))) return false;
        for (int i = 1; i < n; i++) {
            c = s.charAt(i);
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
    
    /**
     * Replace spaces and comment characters in string literals '...' by 
     * (char)0 and (char)1 characters, respectively (which
     * in Java do not terminate strings and can be part of strings).
     * @param s
     * @return s with escaped spaces in string literals
     */
    public static String escapeStrings(String s) {
        int n = s.length();
        StringBuilder sb = new StringBuilder(n);
        boolean inString = false;
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c == '\'') {
                inString = !inString;
                sb.append(c);
            } else if (inString && c == ' ') {
                sb.append((char)0); // 0 is escape character for space character
            } else if (inString && c == '#') {
                sb.append((char)1); // 1 is escape character for # character
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    /**
     * Replaces quote characters ((char)0 and (char)1) in strings.
     * @param s
     * @return s without quote characters
     */
    public static String unescapeStrings(String s) {
        int n = s.length();
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c == 0) {
                sb.append(' ');
            } else if (c == 1) {
                sb.append('#');
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    /**
     * Removes comments from source code. Comments start with # and go to the
     * end of the same line.
     * @param s source code with comments
     * @return source code without comments
     */
    public static String removeComments(String s) {
        return s.replaceAll("(?m)#.*$", ""); // (?m) for multiline mode
    }
    
}

package postfix;

/**
 * Represents the operand stack.
 * @author michaelrohs
 */
public class OperandStack {
    private final Obj[] stack = new Obj[1024];
    private int stackIndex = 0;
    
    public void push(Obj o) {
        if (stackIndex >= stack.length) {
            throw new RuntimeException("Trying to push onto full stack!");
        }
        stack[stackIndex++] = o;
    }
    
    public Obj pop() {
        if (stackIndex <= 0) {
            throw new RuntimeException("Trying to pop from empty stack!");
        }
        return stack[--stackIndex];
    }
    
    public Obj get(int i) {
        if (i < 0 || i >= stackIndex) {
            throw new RuntimeException("OperandStack.get index out of bounds: " + i);
        }
        return stack[i];
    }
    
    public int count() {
        return stackIndex;
    }

    public void clear() {
        stackIndex = 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stackIndex; i++) {
            sb.append(stack[i].toString());
            if (i < stackIndex - 1) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
    
}

package postfix;

import prog1.base.Base;

// javac -cp .:prog1javalib.jar postfix/*.java
// java -cp .:prog1javalib.jar postfix.PostFix
// rm postfix/*.class

/*
 PostFix -- A simple programming language based on postfix notation and arrays.

 Operation:
 There is an operand stack. Operands are placed on the operand stack before a
 command consumes them. Commands consume their operands (except .s, see below).

 Example program 1:
 1 2 +

 Operation of example program 1:
 1: Push 1 on operand stack.                            1
 2: Push 2 on operand stack.                1 2
 +: Pop the topmost two elements from operand stack and add them.
 Push the result back on the operand stack:     3

 Features:
 - Int constants: 1234
 - String constants: 'abc' (note the single quotes)
 - Int arithmetics with +, -, * /
 - Comparison operators: =, <, > (produce integers, like in C)
 - Logical operators: not, or, and (work on integers, like in C)
 - Print last object (and pop from stack): .
 - Print stack (do not modify the stack): .s
 - Random numbers: n rand (generate a random number between 0 and n-1)
 - Stack manipulation: pop, swap, dup, i copy, clear    (for details see below)
 - Standard input: read-int, read-char
 - Arrays: { ... }
 - If-statement: cond { ... } { ... } if        (for details see below)
 - While-loop: cond { ... } while           (for details see below)
 - Define variables/procedures: obj name!: Puts obj into a dictionary.
 Can later be looked up by name.
 */
/**
 * PostFix -- A simple programming language based on postfix notation and
 * arrays.
 */
public class PostFix {

    private final OperandStack stack = new OperandStack();
    private final NamedObjects operations = new NamedObjects();
    private final NamedObjects definedObjects = new NamedObjects();
    public final static int DEBUG_LEVEL = 0;

    public PostFix() {
        // When executed, these operations have an effect on the stack.
        operations.put(".", new PrintTop());    // print topmost element of stack
        operations.put(".s", new PrintStack()); // print stack (don't change it)
        operations.put("+", new Plus());        // a, b --> a + b
        operations.put("-", new Minus());       // a, b --> a - b
        operations.put("*", new Mul());         // a, b --> a * b
        operations.put("/", new Div());         // a, b --> a / b (integer division)
        operations.put("=", new EQ());          // a, b --> if a == b: 1 else: 0
        operations.put("<", new LT());          // a, b --> if a < b:  1 else: 0
        operations.put(">", new GT());          // a, b --> if a > b:  1 else: 0
        operations.put("and", new And());       // a, b --> a and b (0 is false, not 0 is true)
        operations.put("or", new Or());         // a, b --> a or b (0 is false, not 0 is true)
        operations.put("not", new Not());       // a --> not(a)
        operations.put("swap", new Swap());     // a, b --> b, a
        operations.put("pop", new Pop());       // a -->
        operations.put("dup", new Dup());       // a --> a a
        operations.put("copy", new Copy());     // an ... a0 i --> an ... a0 ai
        operations.put("clear", new Clear());   // ... -->
        operations.put("{", new ArrayOpen());   // --> {
        operations.put("}", new ArrayClose());  // { ... --> {...} (new array)
        operations.put("if", new If());         // cond {...} {...} if --> x
                                                // if condition is true (not 0),
                                                // then execute first array,
                                                // else execute second array,
                                                // x is result of array execution
        operations.put("while", new While());   // cond {...} while --> x
                                                // if condition is true execute array,
                                                // if at array end topmost element
                                                // is true execute array again,
                                                // if not do not repeat
        operations.put("rand", new Rand());     // n --> rand(n): produce a random number
                                                // between 0 (inclusive) and n (exclusive)
        operations.put("read-int", new ReadInt());  // read an integer number from stdin
        operations.put("read-char", new ReadChar());// read a single character from stdin
        operations.put("length", new Length()); // a --> length (number of elements)
        operations.put("get", new Get());       // a i --> a[i] (get i-th element)
        operations.put("set", new Set());       // a i v --> a[i]=v (set i-th element to v)
        operations.put("cond", new Cond());     // {...} cond --> execution of the
                                                // first matching condition

        operations.put("run", new Run(this));   // 'file.pf' (executes file)
	operations.put("buildString", new BuildString());
	operations.put("sumCheck", new SumCheck());
    }

    public String interpret(String sourceCode) {
        sourceCode = Util.escapeStrings(sourceCode);
        sourceCode = Util.removeComments(sourceCode);
        String[] tokens = sourceCode.split("[ \n\r]+");
        int openArrays = 0; // the number of currently open arrays

        for (String token : tokens) {
            token = Util.unescapeStrings(token).trim();
            if (token.length() <= 0) {
                continue;
            }
            if (DEBUG_LEVEL >= 3) {
                Base.println("|" + token + "|");
            }
            // check if token is a built-in operation
            Obj o = operations.get(token);
            // if null, then token is not a built-in operation
            if (o == null) {
                // is it an integer?
                if (Util.isInteger(token)) {
                    o = new Int(Base.intOf(token));
                } // is it a string?
                else if (token.length() >= 2 && token.startsWith("'") && token.endsWith("'")) {
                    String s = token.substring(1, token.length() - 1); // remove quotes
                    o = new Str(s);
                } // is it a definition? (ending with '!')
                else if (token.length() >= 2 && token.endsWith("!")) {
                    String name = token.substring(0, token.length() - 1); // remove '!'
                    o = new Def(name, definedObjects);
                }
            }
            // if still null, then treat token as a reference
            if (o == null) {
                o = new Ref(token, definedObjects);
            }
            // assert: o != null
            if (openArrays <= 0 || o instanceof ArrayOpen || o instanceof ArrayClose) {
                if (o instanceof ArrayOpen) {
                    openArrays++;
                }
                if (o instanceof ArrayClose) {
                    openArrays--;
                }
                if (DEBUG_LEVEL >= 2) {
                    Base.println("Executing " + o);
                }
                // if not within array (or "{" or "}"), execute immediately
                o.execute(stack);
            } else {
                // if within array, don't execute, but just push on stack
                if (DEBUG_LEVEL >= 2) {
                    Base.println("Pushing " + o);
                }
                stack.push(o);
            }
            if (DEBUG_LEVEL >= 1) {
                Base.println(stack.toString());
            }
            if (DEBUG_LEVEL >= 3) {
                Base.stringInput(); // wait for return key
            }
        }

        return stack.toString();
    }

    private static PostFix pf() {
        return new PostFix();
    }

    private static void test() {
        String sourceCode, expectedResult, result;

        sourceCode = "'hello world' .";
        expectedResult = "";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "'hello world' .";
        expectedResult = "";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 2 +";
        expectedResult = "3";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 2 3 .s";
        expectedResult = "1 2 3";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 2 + 'hello world' . .s";
        expectedResult = "3";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 2 + dup clear 'hello world' 123 456 .s";
        expectedResult = "'hello world' 123 456";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 { 2 } { 3 } if .s";
        expectedResult = "2";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "0 { 2 } { 3 } if .s";
        expectedResult = "3";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 { 2 dup } { 3 dup } if .s";
        expectedResult = "2 2";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "0 { 2 dup } { 3 dup } if .s";
        expectedResult = "3 3";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "10 dup { 1 - dup dup } while pop .s";
        expectedResult = "9 8 7 6 5 4 3 2 1 0";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        //sourceCode = "read-int 1 + ."; expectedResult = "";
        //result = pf().interpret(sourceCode);
        //Base.checkExpect(result, expectedResult);
        //sourceCode = "1 { read-int dup . } while .s"; expectedResult = "";
        //result = pf().interpret(sourceCode);
        //Base.checkExpect(result, expectedResult);
        sourceCode = "{ dup * } f! 3 f .s";
        expectedResult = "9";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "2 x! x .s";
        expectedResult = "2";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 2 } x! x .s";
        expectedResult = "2";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "'The result is:' { 2 * } f! 10 f .s";
        expectedResult = "'The result is:' 20";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 x! x { x 1 + x! } { x 1 - x! } if x .s";
        expectedResult = "2";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "0 x! x { x 1 + x! } { x 1 - x! } if x .s";
        expectedResult = "-1";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 2 + } add2! 8 add2 .s";
        expectedResult = "10";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ y! x! x y > { x } { y } if } max!   33 3 max .s";
        expectedResult = "33";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        //sourceCode = Base.sReadFile("guess-number.pf"); expectedResult = "";
        //result = pf().interpret(sourceCode);
        //Base.checkExpect(result, expectedResult);

        sourceCode = "'hello world' length { 1 2 3 } length .s";
        expectedResult = "11 3";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 1 2 3 } dup 2 get swap 0 get .s";
        expectedResult = "3 1";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 1 2 'hello world' } dup 2 get swap 0 get .s";
        expectedResult = "'hello world' 1";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ { 1 2 3 } } a! a 2 get a 0 get .s";
        expectedResult = "3 1"; // arrays are executed when referenced, thus needs to put it in another array
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "'aAzZ' dup 0 get swap 1 get .s";
        expectedResult = "97 65";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "'aAzZ' 0 98 set .s";
        expectedResult = "'bAzZ'";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "'aAzZ' dup 0 98 set .s";
        expectedResult = "'aAzZ' 'bAzZ'"; // strings are immutable
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 1 2 3 } 0 98 set .s";
        expectedResult = "{ 98 2 3 }";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 1 2 3 } dup 0 98 set .s";
        expectedResult = "{ 98 2 3 } { 98 2 3 }"; // arrays are mutable
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = Base.sReadFile("to-upper.pf");
        expectedResult = "'HELLO WORLD!?'";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1000 x! { { x 100 > } { 'large' } { x 10 > } { 'medium' } { 1 } { 'small' } } cond .s";
        expectedResult = "'large'"; // arrays are mutable
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "50 x! { { x 100 > } { 'large' } { x 10 > } { 'medium' } { 1 } { 'small' } } cond .s";
        expectedResult = "'medium'"; // arrays are mutable
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "3 x! { { x 100 > } { 'large' } { x 10 > } { 'medium' } { 1 } { 'small' } } cond .s";
        expectedResult = "'small'"; // arrays are mutable
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = Base.sReadFile("conditional.pf");
        expectedResult = "'large' 'medium' 'small'";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        //sourceCode = Base.sReadFile("guess-number-cond2.pf"); expectedResult = "";
        //result = pf().interpret(sourceCode);
        //Base.checkExpect(result, expectedResult);
        sourceCode = Base.sReadFile("tree.pf");
        expectedResult = "";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "'#abc#'#abc";
        expectedResult = "'#abc#'"; // comments are removed, but not in strings
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "1 1 = 1 2 = 'aa' 'aa' = 'aa' 'ab' =";
        expectedResult = "1 0 1 0";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 1 } { 1 } = { 1 1 } { 1 2 } = { 1 } { 'aa' } =";
        expectedResult = "1 0 0";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ dup } { dup } =";
        expectedResult = "1"; // compares unevaluated Dup objects
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "{ 'dup' } { dup } =";
        expectedResult = "0"; // compares a Str object to a Dup object
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = Base.sReadFile("factorial.pf");
        expectedResult = "";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

        sourceCode = "'factorial.pf' run";
        expectedResult = "";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

	sourceCode = "{ a! a a * } square! 4 square";
        expectedResult = "16";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);

	sourceCode = "{ y! x! x y < { x } { y } if } min!   33 3 min .s";
        expectedResult = "3";
        result = pf().interpret(sourceCode);
        Base.checkExpect(result, expectedResult);
    }

    public static void main(String[] args) throws Exception {
        test();
        if (true) {
            PostFix pf = new PostFix();
            String line;
            Base.print("> ");
            while (!(line = Base.stringInput()).equals("quit")) {
                String result = pf.interpret(line);
                Base.println(result);
                Base.print("> ");
            }
        }
    }

}

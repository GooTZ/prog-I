package postfix;

class NamedObject {

    protected final String name;
    protected Obj object;

    public NamedObject(String key, Obj value) {
        this.name = key;
        this.object = value;
    }

}

/**
 * A dictionary of name-value pairs.
 * @author michaelrohs
 */
public class NamedObjects {
    
    private final NamedObject[] values = new NamedObject[1024];
    private int valuesIndex = 0;
    
    public Obj get(String name) {
        NamedObject no = getNameObject(name);
        return no == null ? null : no.object;
    }
    
    private NamedObject getNameObject(String name) {
        for (int i = 0; i < valuesIndex; i++) {
            NamedObject no = values[i];
            if (name.equals(no.name)) {
                return no;
            }
        }
        return null;
    }
    
    public void put(String name, Obj o) {
        NamedObject no = getNameObject(name);
        if (no == null) {
            if (valuesIndex >= values.length) {
                throw new RuntimeException("NamedObjects is full!");
            }
            // new entry
            values[valuesIndex++] = new NamedObject(name, o);
        } else {
            // update existing entry
            no.object = o;
        }
    }
    
}

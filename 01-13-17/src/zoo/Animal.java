public class Animal {

    protected float weight;

    public Animal(float weight) {
        this.weight = weight;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return String.format("Animal (weight=%.2f)", weight);
    }
}

public class Dog extends Animal {

    private float collarLength;

    public Dog(float weight, float collarLength) {
        super(weight);
        this.collarLength = collarLength;
    }

    // Getter/Setter

    @Override
    public String toString() {
        return String.format("Dog (weight=%.2f, collarLength=%.2f)", weight, collarLength);
    }
}

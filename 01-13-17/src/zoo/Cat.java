public class Cat extends Animal {

    private boolean likesWool;

    public Cat(float weight, boolean likesWool) {
        super(weight);
        this.likesWool = likesWool;
    }

    // Getter/Setter...

    @Override
    public String toString() {
        return String.format("Cat (weight=%.2f, likesWool=%b)", weight, likesWool);
    }
}

public class WhiteShark extends Animal {

    private boolean hungry;
    private int surferCount;

    public WhiteShark(float weight, boolean hungry, int surferCount) {
        super(weight);
        this.hungry = hungry;
        this.surferCount = surferCount;
    }

    // Getter/Setter

    public void setSurferCount(int surferCount) {
        this.surferCount = surferCount;
    }

    public int getSurferCount() {
        return surferCount;
    }

    @Override
    public float getWeight() {
        return weight + 70f * surferCount;
    }

    @Override
    public String toString() {
        return String.format("WhiteShark (weight=%.2f, hungry=%b, surferCount=%d)", weight, hungry, surferCount);
    }
}

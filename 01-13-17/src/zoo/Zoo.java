import java.util.List;
import java.util.LinkedList;
import java.util.StringJoiner;

/**
 * Outsourced Animal, Cat, Dog, WhiteShark (uncommon bad design... Style Guidelines)
 * <p>
 * https://google.github.io/styleguide/javaguide.html
 * @author Lars Wrenger
 * @author Domenik Weber
 * @version 0.0.1
 */
public class Zoo {

    public static void main(String[] args) {
        Zoo zoo = new Zoo();

        zoo.addAll(
            new Cat(12.5f, true),
            new WhiteShark(129f, true, 12/*Integer.MAX_VALUE*/),
            new Dog(19f, 10.119f),
            new Cat(-3f, false)
        );

        System.out.println(zoo.toString() + ": ");

        zoo.printAnimals();

        System.out.println("sumWeights(): " + zoo.sumWeights());
    }

    private List<Animal> animals = new LinkedList<>();

    public Zoo() {}

    /**
      * Add an animal to the zoo
      */
    public <T extends Animal> void add(T animal) {
        animals.add(animal);
    }

    /**
      * Add a collection of animals to the zoo
      */
    public void addAll(Animal... animals) {
        for (Animal animal : animals) {
            add(animal);
        }
    }

    /**
      * Outputs all animals to the console
      */
    public void printAnimals() {
        StringJoiner out = new StringJoiner(",\n\t", "Animals [\n\t", "\n]");
        animals.forEach((animal) -> out.add(animal.toString()));
        System.out.println(out.toString());
    }

    /**
      * Caluculate the total weight of all animals in the zoo
      */
    public float sumWeights() {
        float sum = 0f;

        for (Animal animal : animals) {
            sum += animal.getWeight();

            // Implemented in WhiteShark
            // if(animal instanceof WhiteShark) {
            //     sum += 70 * ((WhiteShark) animal).getSurferCount();
            // }
        }

        return sum;
    }

    @Override
    public String toString() {
        return "Zoo ()";
    }
}

import prog1.base.Base;
import prog1.graphics.*;
import static prog1.graphics.Graphics.*;
import java.util.Arrays;

// Windows:
// javac -cp .;prog1javalib.jar UseMap.java
// java -cp .;prog1javalib.jar UseMap

// OS X:
// javac -cp .:prog1javalib.jar UseMap.java
// java -cp .:prog1javalib.jar UseMap

public class UseMap extends javafx.application.Application {

    private BucketMap map = new BucketMap();
    private String lastEvent = "";

    public UseMap() {
        mapTest();
    }

    private void mapTest() {
        map.put("dog", "Hund");
        map.put("whale", "Wal");
        map.put("cow", "Kuh");
        map.put("donkey", "Esel");
        map.put("22", "44");
        map.put("3", "6");
        map.put("13", "26");
        map.put("9", "18");

        Base.checkExpect(map.getAllKeys() != null ? map.getAllKeys().length : 0, 8);
        Base.checkExpect(map.getAllValues() != null ? map.getAllValues().length : 0, 8);
        // Debug: System.out.println(Arrays.toString(map.getAllValues()));
        Base.checkExpect(map.contains("whale"), true);
        Base.checkExpect(map.contains("3"), true);
        Base.checkExpect(map.contains("Trump"), false);
        map.setColor("3", "red");
        Base.checkExpect(map.getKeyValue("3").color, "red");
        Base.checkExpect(map.getKeyValue("9").color, "black");
        Base.checkExpect(map.sumAllValues(), 44+6+26+18);


        System.out.print("\nWanna find something in the map? Start typing and press Enter to search.\n");
    }

    @Override
    public void start(javafx.stage.Stage stage) { // entry point
        // window title, width, and height; drawing method
        ApplicationBase.start("UseMap", 800, 600, stage, this::onDraw);
        ApplicationBase.setOnKeyPressed(this::onKeyPress);
    }

    private Image onDraw() {
        Image i = map.toImage();
        i = above(i, text(lastEvent, 12, "black"));
        // i.save("my.png");
        return i;
    }

    private void onKeyPress(String event) {
        // press Space to add random numeric elements to Map
        // Debug: System.out.println(event);
        if(event != null){
            if(event.matches(" ")) {
                int x = Base.rnd(100);
                int y = Base.rnd(200);
                map.put("" + x, "" + y);
            } else if(event.matches("[a-zA-Z_0-9,.]")) {
                lastEvent += event;
                System.out.print("\rWanna find some '"+lastEvent+"'? Press Enter.");
            } else if(event.matches("BACK_SPACE")) {
                if(lastEvent.length() > 0) {
                    lastEvent = lastEvent.substring(0, lastEvent.length() - 1);
                    System.out.print("\rWanna find some '"+lastEvent+"'? Press Enter.");
                }
            } else if (!event.matches("SHIFT")) {
                // search for searchstring in map, color item if found
                if(map.contains(lastEvent)) {
                    map.setColor(lastEvent, "red");
                    System.out.println("\nFound '"+lastEvent+"'. Marked in red now.");
                } else {
                    System.out.println("\nCould not find '"+lastEvent+"'.");
                }
                lastEvent = "";
            }
        }
    }

}

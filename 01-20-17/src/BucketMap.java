import static prog1.functional.Functional.*;
import static prog1.graphics.Graphics.*;
import prog1.graphics.Image;
import java.util.ArrayList;

public class BucketMap {

    private final static int N = 10; // should be more buckets in practice
    private final KeyValue[] buckets = new KeyValue[N];

    private int hash(String key) {
        int n = key.length();
        int hash = 0;
        for (int i = 0; i < n; i++)
            hash = hash * 31 + key.charAt(i); // compute index from characters
        if (hash < 0) hash = -hash;
        return hash;
    }

    public void put(String key, String value) {
        if (contains(key)) {
            getKeyValue(key).value = value;
        }
        else {
            int h = hash(key) % N;
            // add key-value association to front of list in bucket h
            buckets[h] = new KeyValue(key, value, buckets[h]);
        }
    }

    public KeyValue getKeyValue(String key) {
        int h = hash(key) % N;
        // walk through list in bucket h
        for (KeyValue kv = buckets[h]; kv != null; kv = kv.next)
            if (key.equals(kv.key))
                return kv; // found key, return kv
        return null; // key not found, return null
    }

    public String get(String key) {
        KeyValue kv = getKeyValue(key);
        return kv != null ? kv.value : null;
    }

    public boolean contains(String key) {
        return getKeyValue(key) != null;
    }

    public String[] getAllKeys() {
        ArrayList<String> list = new ArrayList<>();
        for (KeyValue keyValue : buckets) {
            while(keyValue != null) {
                list.add(keyValue.key);
                keyValue = keyValue.next;
            }
        }
        return list.toArray(new String[list.size()]);
    }

    public String[] getAllValues() {
        ArrayList<String> list = new ArrayList<>();
        for (KeyValue keyValue : buckets) {
            while(keyValue != null) {
                list.add(keyValue.value);
                keyValue = keyValue.next;
            }
        }
        return list.toArray(new String[list.size()]);
    }

    public void setColor(String key, String color) {
        KeyValue keyValue = getKeyValue(key);
        if (keyValue != null) 
            keyValue.color = color;
    }

    public int sumAllValues() {
        int sum = 0;
        String[] values = getAllValues();
        for (String value : values) {
            sum += getIntValue(value);
        }
        return sum;
    }

    private int getIntValue(String s) {
        try {
           return Integer.parseInt(s);
        }
        catch (NumberFormatException squish) {
            // this is not an int
            return 0;
        }
    }

    private final static Image arrow = rotate(90, polygon(array(
                -2.0, 0, -2, 10, -6, 10, 0, 20, 6, 10, 2, 10, 2, 0),
                pen("blue")));

    private Image bucketToImage(int i) {
        KeyValue kv = buckets[i];
        Image result = overlay(
                text("" + i, 12, "black"),
                rectangle(60, 25, pen("black")));
        for (KeyValue n = kv; n != null; n = n.next) {
            result = beside(result, arrow, n.toImage());
        }
        return result;
    }

    public Image toImage() {
        Image result = space(0);
        for (int i = 0; i < buckets.length; i++) {
            result = above("left", result, bucketToImage(i));
        }
        return result;
    }

}

import prog1.graphics.Image;
import static prog1.graphics.Graphics.*;

public class KeyValue {
    public String key; // e.g., dog
    public String value; // e.g., Hund
    public KeyValue next; // link to next element in same bucket
    public String color = "black";

    public KeyValue(String key, String value, KeyValue next) {
        this.key = key; 
        this.value = value;
        this.next = next;
    }
    
    public Image toImage() {
        String s = key + ", " + value;
        return overlay(text(s, 12, color), rectangle(100, 25, pen(color)));
    }
}

import prog1.base.Base;

// Windows:
// javac -cp .;prog1javalib.jar BinarySearchTree.java
// java -cp .;prog1javalib.jar BinarySearchTree

// OS X:
// javac -cp .:prog1javalib.jar BinarySearchTree.java
// java -cp .:prog1javalib.jar BinarySearchTree

public class BinarySearchTree {
    private class Node {
        private int value;
        private Node left, right;

        private Node(int value, Node left, Node right) {
            this.left = left;
            this.value = value;
            this.right = right;
        }

        private Node(int value) {
            this(value, null, null);
        }

        private void add(int i) {
            if (i < value) {
                if (left == null) {
                    left = new Node(i);
                } else {
                    left.add(i);
                }
            } else {   // n >= value
                if (right == null) {
                    right = new Node(i);
                } else {
                    right.add(i);
                }
            }
        }

        private int size() {
            int size = 1;

            if (left != null) {
                size += left.size();
            }
            if (right != null) {
                size += right.size();
            }
            return size;
        }

        private boolean contains(int i) {
            if (i < value) {
                return left == null ? false : left.contains(i);
            }
            if (i > value) {
                return right == null ? false : right.contains(i);
            }
            return true;
        }

        private int numberOfLeaves() {
            if (left == null && right == null) {
                return 1;
            }

            int i = 0;

            if (left != null) {
                i += left.numberOfLeaves();
            }
            if (right != null) {
                i += right.numberOfLeaves();
            }

            return i;
        }

        private int nearest(int target, int nearestSoFar) {
            int distanceSoFar = Math.abs(target - nearestSoFar);
            int distance = Math.abs(target - value);
            int nearest = distanceSoFar < distance ? nearestSoFar : value;

            // Debug: System.out.println(value+": "+distanceSoFar + ", "+ distance + ", " + nearest);

            if (target < value && left != null) {
                return left.nearest(target, nearest);
            }
            if (target > value && right != null) {
                return right.nearest(target, nearest);
            }

            return nearest;
        }

        public String toString() {
            if (left == null && right == null) {
                return String.valueOf(value);
            }
            return "(" + (left != null ? left : "_") +
                   "," + value + "," +
                   (right != null ? right : "_") + ")";
        }
    } // class Node

    private Node root = null;

    public void add(int i) {
        if (root == null) {
            root = new Node(i);
        } else {
            root.add(i);
        }
    }

    public int size() {
        if (root == null) {
            return 0;
        } else {
            return root.size();
        }
    }

    public boolean contains(int i) {
        if (root == null) {
            return false;
        } else {
            return root.contains(i);
        }
    }

    public int numberOfLeaves() {
        if (root == null) {
            return 0;
        } else {
            return root.numberOfLeaves();
        }
    }

    public String toString() {
        if (root == null) {
            return "()";
        } else {
            return root.toString();
        }
    }

    public int nearest(int value) {
        if (root == null) {
            return Integer.MAX_VALUE;
        } else {
            return root.nearest(value, Integer.MAX_VALUE);
        }
    }

    private static void test() {
        BinarySearchTree tree = new BinarySearchTree();

        int[] values = { 1, 3, -3, -64, 11, 50, -52, 24, -43, 37 };
        for (int v : values) {
            tree.add(v);
        }
        //   Tree structure
        //          1
        //     -3       3
        // -64              11
        //    -52               50
        //  -43               24
        //                      37
        Base.println(tree);
        Base.checkExpect(tree.contains(values[2]), true);
        Base.checkExpect(tree.contains(values[2] + 1000), false);
        Base.checkExpect(tree.size(), values.length);
        Base.checkExpect(tree.numberOfLeaves(), 2);
        Base.checkExpect(tree.nearest(3), 3);
        Base.checkExpect(tree.nearest(36), 37);
        Base.checkExpect(tree.nearest(0), 1);
        Base.checkExpect(tree.nearest(-100), -64);
        Base.summary();
    }

    public static void main(String[] args) {
        test();
    }
}

/*
Compile: make mensa
Run: ./mensa
Compile & run: make mensa && ./mensa
*/

#include "base.h"		// http://hci.uni-hannover.de/files/prog1lib/base_8h.html
#include "string.h"	  // http://hci.uni-hannover.de/files/prog1lib/string_8h.html
#include "list.h"		// http://hci.uni-hannover.de/files/prog1lib/list_8h.html
#include "string_list.h" // http://hci.uni-hannover.de/files/prog1lib/string__list_8h.html

#define CNRM  "\x1B[0m"
#define CRED  "\x1B[31m"
#define CGRN  "\x1B[32m"
#define CYEL  "\x1B[33m"
#define CBLU  "\x1B[34m"
#define CMAG  "\x1B[35m"
#define CCYN  "\x1B[36m"
#define CWHT  "\x1B[37m"

/**
Today's menu.
*/
const String menu[] = { "Currywurst", "Spaghetti Bolognese", "Vegi", "Soup", "Salad" };

/**
The number of items on the menu.
*/
const int menu_count = sizeof(menu) / sizeof(char*);

/**
The list of completed food from the kitchen, waiting to be served. Each time a food item is served, the kitchen prepares a new food item randomly from the menu.
*/
List food; // List(String)

/**
The list of students standing in line and waiting for food. Each entry contains a string for the food that this student wants. The student at index 0 is at the front of the list.
*/
List students; // List(String)

/**
The reputation of the mensa. The goal of the player is to maximize the reputation of the mensa. Serving a dish that was asked for increases the reputation of the mensa by 1. Serving the wrong dish decreases the reputation by 1. If a student asks for a dish that is on the menu but not ready to be served, the reputation of the mensa decreases by 2.
*/
int reputation = 0;

int running = 1;

String get_color_from_food(String food)
{
	if (!strcmp(food, menu[0])) {
		return CYEL;
	} else if (!strcmp(food, menu[1])) {
		return CCYN;
	} else if (!strcmp(food, menu[2])) {
		return CGRN;
	} else if (!strcmp(food, menu[3])) {
		return CMAG;
	} else if (!strcmp(food, menu[4])) {
		return CBLU;
	} else
		return CNRM;
}

String str_append(String s1, String s2, String s3)
{
	String clr_s;
	if((clr_s = malloc(strlen(s1)+strlen(s2)+strlen(s3)+1)) != NULL){
		strcat(clr_s, s1);
		strcat(clr_s, s2);
		strcat(clr_s, s3);
	} else {
		printf("malloc failed!\n");
		exit(EXIT_FAILURE);
	}

	return clr_s;
}

String get_student(int i)
{
	String s = sl_get(students, i);

	return str_append(get_color_from_food(s), s, CNRM);
}

String c_sl_get(List l, int i)
{
	String s = sl_get(l, i);
	return str_append(get_color_from_food(s), s, CNRM);
}

void c_sl_println(List l)
{
	int length = l_length(l);
	String color = CWHT;
	printf("[");
	for (int i = 0; i < length; i++) {
		color = get_color_from_food(sl_get(l, i));
		printf(" %s %s %s,", color, sl_get(l, i), CNRM);
	}
	printf("%s]\n", CNRM);
}

/**
food: [Currywurst, Salad, Spaghetti Bolognese, Vegi, Salad]
next student: Vegi (3 students waiting)
mensa reputation: 0
>
*/

void sl_pop_front(List l)
{
	sl_remove(l, 0);

}

void step_food_line(int input)
{
	sl_remove(food, input - 1);
	sl_pop_front(students);
	sl_append(students, menu[i_rnd(menu_count)]);
	sl_append(food, menu[i_rnd(menu_count)]);
}


void print_situation(void)
{
	printf("food: "); c_sl_println(food);
	if (l_length(students)) {
		printf("next student: %s (%i students waiting)\n",
			c_sl_get(students, 0), l_length(students));
	} else {
		printf("No more students. Done for today. The mensa is closed.\n");
	}

	printf("mensa reputation: %i\n", reputation);
}

void take_action_from_input(int input)
{
	if (input == -2) {
		running = 0;
		return;
	}

	if (input == -1){
		reputation -= 2;
		printf("You don't have %s? What a crappy mensa!\n", c_sl_get(students, 0));
		sl_pop_front(students);
	} else if (input < 6 && input > 0) {
		if (sl_get(students, 0) == sl_get(food, input - 1)) {
			reputation += 1;
			printf("Thank you!\n");
			step_food_line(input);
		} else {
			printf("I don't want %s, I want %s!\n", c_sl_get(food, input - 1), c_sl_get(students, 0));
			reputation -= 1;
		}
	} else {
		printf("%i is not a valid input, try again!\n", input);
		return;
	}


}

/**
Print final message, release all dynamically allocated memory, and exit the program.
*/
void finish(void) {
	l_free(food);
	l_free(students);
}

/**
Run the mensa simulation.

The mensa person then enters the (0-based) index from the list of food. -1 stands for "not available". -2 ends the program.

Here is an example dialog:

food: [Currywurst, Salad, Spaghetti Bolognese, Spaghetti Bolognese, Salad]
next student: Vegi (3 students waiting)
mensa reputation: 0
> 0
I don't want Currywurst! I want Vegi!
food: [Currywurst, Salad, Spaghetti Bolognese, Spaghetti Bolognese, Salad]
next student: Vegi (3 students waiting)
mensa reputation: -1
> -1
You don't have Vegi? What a crappy mensa!
food: [Currywurst, Salad, Spaghetti Bolognese, Spaghetti Bolognese, Salad]
next student: Salad (2 students waiting)
mensa reputation: -3
> 1
Thank you!
food: [Currywurst, Spaghetti Bolognese, Spaghetti Bolognese, Salad, Currywurst]
next student: Vegi (2 students waiting)
mensa reputation: -2
> -1
You don't have Vegi? What a crappy mensa!
food: [Currywurst, Spaghetti Bolognese, Spaghetti Bolognese, Salad, Currywurst]
next student: Currywurst (1 students waiting)
mensa reputation: -4
> 0
Thank you!
food: [Spaghetti Bolognese, Spaghetti Bolognese, Salad, Currywurst, Salad]
next student: Spaghetti Bolognese (1 students waiting)
mensa reputation: -3
> 0
Thank you!
food: [Spaghetti Bolognese, Salad, Currywurst, Salad, Currywurst]
next student: Currywurst (1 students waiting)
mensa reputation: -2
> 4
Thank you!
food: [Spaghetti Bolognese, Salad, Currywurst, Salad, Soup]
next student: Vegi (1 students waiting)
mensa reputation: -1
> -1
You don't have Vegi? What a crappy mensa!
No more students. Done for today. The mensa is closed.
Final reputation: -3
*/
void run_mensa(void)
{
	// todo: implement

	// create 5 random food items from the menu
	// store in list...
	food = sl_create();
	for (int i = 0; i < menu_count; i++) {
		sl_append(food, menu[i_rnd(menu_count)]);
	}

	// create 3 random food wishes from the menu (each wish from one student)
	// store in list...
	students = sl_create();
	for (int i = 0; i < 3; i++) {
		sl_append(students, menu[i_rnd(menu_count)]);
	}

	// in a loop: read user input, take action based on input, print situation
	//...
	print_situation();
	while(l_length(students) && running) {
		take_action_from_input(i_input());
		print_situation();
	}

	finish();
}

int main(void)
{
	base_init();
	base_set_memory_check(true);
	run_mensa();
	return 0;
}

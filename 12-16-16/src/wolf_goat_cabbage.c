/*
   Compile: make wolf_goat_cabbage
   Run: ./wolf_goat_cabbage
   make wolf_goat_cabbage && ./wolf_goat_cabbage
 */

#include "base.h"        // http://hci.uni-hannover.de/files/prog1lib/base_8h.html
#include "string.h"      // http://hci.uni-hannover.de/files/prog1lib/string_8h.html
#include "list.h"        // http://hci.uni-hannover.de/files/prog1lib/list_8h.html
#include "string_list.h" // http://hci.uni-hannover.de/files/prog1lib/string__list_8h.html

/**
   List of objects on the left river bank.
 */
List left;

/**
   List of objects on the right river bank.
 */
List right;

/**
   List of objects in the boat. The boat has a capacity of one object only.
 */
List boat;

/**
   The boat may either be at the left or right river bank. We don't care for the transition (boat crossing the river).
 */
enum Position {
    LEFT, RIGHT
};

const String wolf = "\x1B[35mwolf\x1B[0m";
const String goat = "\x1B[33mgoat\x1B[0m";
const String cabbage = "\x1B[36mcabbage\x1B[0m";

/**
   Current boat position.
 */
enum Position position;

void print_situation(void) {
    sl_print(left);
    if(position == RIGHT) {
        prints("\x1B[34m ~ ~ ~ \x1B[0m");
    }
    sl_print(boat);
    if(position == LEFT) {
        prints("\x1B[34m ~ ~ ~ \x1B[0m");
    }
    sl_println(right);
}

void finish(void) {
    l_free(left);
    l_free(right);
    l_free(boat);
}

bool evaluate_situation(void) {
    // wolf - goat
    if((sl_contains(left, wolf) && sl_contains(left, goat) && position == RIGHT) ||
       (sl_contains(right, wolf) && sl_contains(right, goat) && position == LEFT)) {
        print_situation();
        printf("\x1B[31mWolf kills goat!\nGameover\x1B[0m\n");
        finish();
        exit(EXIT_FAILURE);
    }
    // goat - cabbage
    if((sl_contains(left, cabbage) && sl_contains(left, goat) && position == RIGHT) ||
       (sl_contains(right, cabbage) && sl_contains(right, goat) && position == LEFT)) {
        print_situation();
        printf("\x1B[31mGoat eats cabbage!\nGameover\x1B[0m\n");
        finish();
        exit(EXIT_FAILURE);
    }

    return sl_contains(right, cabbage) && sl_contains(right, goat) && sl_contains(right, wolf);
}

bool starts_with(String element, int index, String x) {
    return s_starts_with(element, x);
}

void clearInput() {
    char c;
    while((c = getchar()) != '\n' && c != EOF) {
    }
}

void load_boat(List shore, String object) {
    if(sl_contains(shore, object)) {
        // Add object to the boat
        sl_remove(shore, sl_index(shore, object));
        if(boat->first != NULL) {
            load_boat(shore, sl_get(boat ,0));
        }
        sl_append(boat, object);
    } else if (sl_contains(boat, object)) {
        // Add object to shore
        sl_remove(boat, sl_index(boat, object));
        sl_append(shore, object);
    }
}

bool user_input() {
    char i = getchar();
    clearInput();

    switch(i) {
    case 'g':
        load_boat(position == LEFT ? left : right, goat);
        return 0;
    case 'w':
        load_boat(position == LEFT ? left : right, wolf);
        return 0;
    case 'c':
        load_boat(position == LEFT ? left : right, cabbage);
        return 0;
    case 'q':
        finish();
        exit(EXIT_SUCCESS);
    }
    return 1;
}

void play_wolf_goat_cabbage(void) {
    left = sl_create();
    sl_append(left, goat);
    sl_append(left, wolf);
    sl_append(left, cabbage);
    right = sl_create();
    boat = sl_create();

    int counter = 0;
    while(!evaluate_situation()) {
        print_situation();
        // Evaluate user input
        printf("\x1B[90m[g:goat, w:wolf, c:cabbage, m:move]\x1B[0m$ ");

        // Evaluate user input
        if(user_input()) {
            // Change position
            position = position == LEFT ? RIGHT : LEFT;
            counter++;
        }
    }
    print_situation();
    printf("\x1B[32mSuccess!\n%i steps.\x1B[0m\n", counter);
    finish();
}

int main(void) {
    base_init();
    base_set_memory_check(true);
    play_wolf_goat_cabbage();
    return EXIT_SUCCESS;
}

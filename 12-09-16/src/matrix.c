/*
   Compile: make matrix_solution
   Run: ./matrix_solution
 */

#include "base.h" // http://hci.uni-hannover.de/files/prog1lib/base_8h.html

struct Matrix {
    int rows; // number of rows
    int cols; // number of columns
    double** data; // a pointer to an array of n_rows pointers to rows; a row is an array of n_cols doubles
};
typedef struct Matrix Matrix;

void print_matrix(Matrix* m);

/**
   Creates a zero-initialized matrix of rows and columns matrix.
   @param[in] n_rows number of rows
   @param[in] n_cols number of columns
   @return a pointer to an array of n_rows pointers to rows; a row is an array of n_cols doubles
 */
Matrix* make_matrix(int n_rows, int n_cols) {
    // todo: implement
    if(n_rows > 0 && n_cols > 0) {
        double** rows = xmalloc(sizeof(double*) * n_rows);
        for(size_t i = 0; i < n_rows; i++) {
            *rows++ = xcalloc(n_cols, sizeof(double));
        }
        Matrix* m = xmalloc(sizeof(Matrix));
        m->rows = n_rows;
        m->cols = n_cols;
        m->data = rows - n_rows;
        return m;
    }
    return NULL;
}

/**
   Creates a zero-initialized matrix of rows and columns matrix.
   @param[in] data an array of doubles, ordered row-wise
   @param[in] n_rows number of rows
   @param[in] n_cols number of columns
   @return a pointer to an array of n_rows pointers to rows; a row is an array of n_cols doubles
 */
Matrix* copy_matrix(double* data, int n_rows, int n_cols) {
    Matrix* m = make_matrix(n_rows, n_cols);

    if(m != NULL) {
        for(size_t x = 0; x < n_rows; x++) {
            for(size_t y = 0; y < n_cols; y++) {
                m->data[x][y] = data[x * n_cols + y];
            }
        }
        return m;
    }
    return NULL;
}

/**
   Print a matrix.
   @param[in] m the matrix to print
 */
void print_matrix(Matrix* m) {
    if(m != NULL) {
        for(size_t x = 0; x < m->rows; x++) {
            printf("[");
            for(size_t y = 0; y < m->cols; y++) {
                printf("%6.2f", m->data[x][y]);
                if(y < m->cols - 1) {
                    printf(", ");
                }
            }
            printf("]\n");
        }
    }
    else {
        printf("Null matrix!\n");
    }
    printf("\n");
}

/**
   Add two matrices.
   @param[in] a the first operand
   @param[in] b the second operand
   @return a new matrix whose elements are the element-wise sums of a and b
 */
Matrix* add_matrices(/*in*/ Matrix* a, /*in*/ Matrix* b) {
    if(a->cols == b->cols && a->rows == b->rows) {
        Matrix* m = make_matrix(a->rows, a->cols);
        for(size_t x = 0; x < m->rows; x++) {
            for(size_t y = 0; y < m->cols; y++) {
                m->data[x][y] = a->data[x][y] + b->data[x][y];
            }
        }
        return m;
    }
    return NULL;
}

void swapRows(Matrix* m, int a, int b) {
    for(size_t y = 0; y < m->cols; y++) {
        int temp = m->data[a][y];
        m->data[a][y] = m->data[b][y];
        m->data[b][y] = temp;
    }
    // printf("swapRows()\n");
    // print_matrix(m);
}

void multiplyRow(Matrix* m, int row, double scalar) {
    for(size_t y = 0; y < m->cols; y++) {
        m->data[row][y] *= scalar;
    }
    // printf("multiplyRow() %f\n", scalar);
    // print_matrix(m);
}

void addRows(Matrix* m, int row_a, int scalar_a, int row_b, int scalar_b) {
    for(size_t y = 0; y < m->cols; y++) {
        m->data[row_b][y] = scalar_a * m->data[row_a][y] + scalar_b * m->data[row_b][y];
    }
    // printf("addRows()\n");
    // print_matrix(m);
}

void rref(Matrix* m) {
    int row = 0;
    int col = 0;

    while(row < m->rows && col < m->cols - 1) {
        // printf("while: col %i\n", col);

        // find next value != 0
        int next_row = -1;
        for(size_t x = col; x < m->cols - 1 && next_row < 0; x++) {
            for(size_t y = row; y < m->rows && next_row < 0; y++) {
                if(m->data[y][x] > EPSILON || m->data[y][x] < -EPSILON) {
                    next_row = y;
                    col = x;
                }
            }
        }
        // printf("row:%i x col:%i\n", next_row, col);
        if(next_row < 0) {
            break;
        }

        if(next_row != row) {
            swapRows(m, next_row, row);
        }

        // Get 1
        multiplyRow(m, row, 1.0 / m->data[row][col]);

        for(size_t y = 0; y < m->rows; y++) {
            if(y != row && m->data[y][col]) {
                addRows(m, row, -m->data[y][col], y, 1);
            }
        }

        row++;
        col++;
    }
}

/**
   Free a matrix.
   @param[in] m the matrix to free
 */
void free_matrix(Matrix* m) {
    if(m != NULL) {
        for(size_t i = 0; i < m->rows; i++) {
            free(m->data[i]);
        }
        free(m->data);
        free(m);
    }
}

void matrix_test(void) {
    double a[] = {
        1, 2, 3, -2, 1, 4,
        3, 6, 5, -4, 3, 5,
        1, 2, 7, -4, 1, 11,
        2, 4, 2, -3, 3, 6
    };
    Matrix* m1 = copy_matrix(a, 4, 6);
    printf("m1\n");
    print_matrix(m1);

    double a2[] = {
        1, 2, 3, 1,
        4, 5, 6, 2,
        7, 8, 9, 3
    };
    Matrix* m2 = copy_matrix(a2, 3, 4);
    printf("m2\n");
    print_matrix(m2);

    double a3[] = {
        1, 2,
        3, 4,
        5, 6
    };
    Matrix* m3 = copy_matrix(a3, 3, 2);
    printf("m3\n");
    print_matrix(m3);

    double a4[] = {
        10, 20,
        30, 40,
        50, 60
    };
    Matrix* m4 = copy_matrix(a4, 3, 2);
    printf("m4\n");
    print_matrix(m4);

    Matrix* m5 = add_matrices(m3, m4);
    printf("m1\n");
    print_matrix(m5);

    printf("gauß-algorithm: m1\n");
    rref(m1);
    print_matrix(m1);

    printf("gauß-algorithm: m2\n");
    rref(m2);
    print_matrix(m2);

    printf("gauß-algorithm: m4\n");
    rref(m4);
    print_matrix(m4);

    free_matrix(m1);
    free_matrix(m2);
    free_matrix(m3);
    free_matrix(m4);
    free_matrix(m5);
}

int main(void) {
    base_init();
    base_set_memory_check(true);
    matrix_test();
    return EXIT_SUCCESS;
}

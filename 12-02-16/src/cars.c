/*
   Compile: make cars
   Run: ./cars
   make cars && ./cars
 */

// No prog1lib allowed! You may only use standard C functions!
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char* model;
    double power; // kW
    double weight; // kg
    double top_speed; // km/h
    double co2; // CO2 emissions in g/km
} Car;

/*
   Constructor function for a Car.
 */
Car make_car(
    char* model,
    double power, // kW
    double weight, // kg
    double top_speed, // km/h
    double co2) {
    Car c = { model, power, weight, top_speed, co2 };

    return c;
}


/*
   Car* -> char**
   Returns the model names of the cars.
 */
char** car_models(Car* cars, int numberOfCars) {
    char** models = malloc(numberOfCars * sizeof(char*));

    for(size_t i = 0; i < numberOfCars; i++) {
        models[i] = cars[i].model;
        // *(models + i) = models[i]
    }
    return models;
}

/*
   char** -> void
   Prints every String in an Array, each on a new line
 */
void print_string_array(char** strings, int numberOfCars) {
#if 0
    for(size_t i = 0; i < numberOfCars; i++) {
        printf("%s\n", strings[i]);
    }
#else
    while(*strings) {   // No need for length?
        printf("%s\n", *strings++);
    }
#endif
}


/*
   Car* -> double
   Computes the sum of CO2 emissions of all cars.
 */
double average_co2_emissions(Car* cars, int numberOfCars) {
    double emission = 0;

    for(size_t i = 0; i < numberOfCars; i++) {
        emission += cars[i].co2;
    }
    return emission / numberOfCars;
}


/*
   Car* -> double*
   Computes the power-to-weight ratios of the cars.
 */
double* power_weight_ratios(Car* cars, int numberOfCars) {
    double* power_weight = malloc(numberOfCars * sizeof(double));

#if 0
    for(size_t i = 0; i < numberOfCars; i++) {
        power_weight[i] = cars[i].power / cars[i].weight;
    }
    return power_weight;
#else
    for(size_t i = 0; i < numberOfCars; i++) {
        *(power_weight++) = cars->power / cars->weight;
        cars++;
    }
    return power_weight - numberOfCars;
#endif
}

/*
   Car*, double -> int
   Returns the number of those cars whose power does not exceed limit.
 */
int power_less_than_count(Car* cars, int numberOfCars, double limit) {
    int num = 0;

    for(size_t i = 0; i < numberOfCars; i++) {
        if((*cars++).power < limit) {
            num++;
        }
    }
    return num;
}

/*
   Car*, double -> Car*
   Returns a new array with only those cars whose power does not exceed limit.
 */
Car* power_less_than(Car* cars, int numberOfCars, double limit) {
    int filtered_length = power_less_than_count(cars, numberOfCars, limit);
    Car* filtered = malloc(filtered_length * sizeof(Car));

    for(size_t i = 0; i < numberOfCars; i++) {
        if((*cars).power < limit) {
            *filtered++ = *cars++;
        }
    }
    return filtered - filtered_length;
}


const int NUMBER_OF_CARS = 6;
int main(void) {
    Car* cars = malloc(NUMBER_OF_CARS * sizeof(Car));

    cars[0] = make_car("Chevrolet Spark", 38, 1070, 144, 161);
    cars[1] = make_car("Citroen C1", 50, 805, 158, 106);
    cars[2] = make_car("Fiat 500", 10, 499, 105, 90);
    cars[3] = make_car("Opel Adam", 110, 1178, 210, 139);
    cars[4] = make_car("Porsche 911 Turbo S", 427, 1675, 330, 212);
    cars[5] = make_car("Bentley Bentayga", 447, 2422, 301, 296);

    printf("models:\n");
    // get models of all cars as a string array
    char** models = car_models(cars, NUMBER_OF_CARS);
    // print this string array
    print_string_array(models, NUMBER_OF_CARS);

    // print the average co2 emissions for all models
    printf("\nThe average CO2 emissions between all cars is %.1f g/km\n", average_co2_emissions(cars, NUMBER_OF_CARS));

    // print the power by weight ratios for all models
    printf("\npower by weight (kW/kg) ratios:\n");
    double* pws = power_weight_ratios(cars, NUMBER_OF_CARS);
    if(pws != NULL) {
        for(int i = 0; i < NUMBER_OF_CARS; i++) {
            printf("%s: %.3f kW/kg\n", models[i], pws[i]);
        }
    }

    // print all models with less than LIMIT power
    double LIMIT = 100.0; // kW

    int low_power_cars_count = power_less_than_count(cars, NUMBER_OF_CARS, LIMIT);
    Car* low_power_cars = power_less_than(cars, NUMBER_OF_CARS, LIMIT);
    printf("\n%d cars have less than %.0f kW:\n", low_power_cars_count, LIMIT);
    for(int i = 0; i < low_power_cars_count; i++) {
        printf("%s\n", low_power_cars[i].model);
    }

    return EXIT_SUCCESS;
}

/*
Compile: make closest
Run: ./closest
*/

// No prog1lib allowed! You may only use standard C functions!
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*
Given two arrays a and b with double values. Implement a function which computes
the index of the most similar element in b for each element of a. Similarity in
this case means the smallest absolute difference. The function shall return an
int array with these indices.
*/

// int*, int, int*, int -> int
int check_int_arrays_match(int* a, int an, int* b);

// check whether array a with length an matches array b with length bn
int check_int_arrays_match(int* a, int an, int* b) {
	int failed_comparisons = 0;
	for (int i = 0; i < an; i++) {
		if (a[i] != b[i]) {
			printf("Test failed: %d is != %d\n", a[i], b[i]);
			failed_comparisons++;
		} else {

		}
	}

	if (!failed_comparisons) {
		printf("Test passed!\n");
	}

	return 1;
}

// // double[], double[] --> int[]
// Returns an array with the indizies of the closest elements in array b
int* closest(double a[], int length_a, double b[], int length_b);

static void closest_test(void) {
	int sizeOfA = 3;
	double test_1_arr1[] = {0.1, 3.5, 2.0};
	double test_1_arr2[] = {1.0, 2.0};
	int test_1_expected[] = {0, 1, 1};
	check_int_arrays_match(closest(test_1_arr1, sizeOfA, test_1_arr2, 2), sizeOfA, test_1_expected);

	double test_2_arr1[] = {0.1, 3.5, 2.0, 1.1};
	double test_2_arr2[] = {1.0, 2.0, 3.0, 4.0};
	int test_2_expected[] = {0, 2, 1, 0};
	check_int_arrays_match(closest(test_2_arr1, 4, test_2_arr2, 4), 4, test_2_expected);

	double test_3_arr1[] = {4.1, 13.5, 22.0};
	double test_3_arr2[] = {5.0, 25.1, 15.5};
	int test_3_expected[] = {0, 2, 1};
	check_int_arrays_match(closest(test_3_arr1, 3, test_3_arr2, 3), 3, test_3_expected);

	double test_4_arr1[] = {1.337, 1.23, 1.2};
	double test_4_arr2[] = {1.1, 1.2, 1.3};
	int test_4_expected[] = {2, 1, 1};
	check_int_arrays_match(closest(test_4_arr1, 3, test_4_arr2, 3), 3, test_4_expected);
}

double fabs(double x) {
	return (x >= 0) ? x : -x;
}

int * closest(double a[], int length_a, double b[], int length_b) {
	int * indices = malloc(length_a * sizeof(int));
	int tmp = 0;
	for (int i = 0; i < length_a; i++) {
		for (int j = 0; j < length_b; j++) {
			// aktuelle differenz kleiner als derzeit kleinste
			if (fabs(a[i] - b[j]) < fabs(a[i] - b[tmp])) {
				tmp = j;
			}
		}
		if (length_b <= 0) {
			indices[i] = -1;
		} else {
			indices[i] = tmp;
		}
	}
	return indices;
}

int main(void) {
	closest_test();
	return EXIT_SUCCESS;
}

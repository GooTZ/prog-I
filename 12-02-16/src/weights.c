/*
Compile: make weights
Run: ./weights
*/

// No prog1lib allowed! You may only use standard C functions!
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// checks whether two doubles are almost equal. Returns 1 (true) if that's the case, 0 (false) otherwise
int check_double_almost_equals(double a, double b) {
	if ((fabs(a) - fabs(b)) < 0.000001) {
		printf("check for number %f passed!\n", a);
		return 1;
	}
	printf("check for numbers %f and %f failed!\n", a, b);
	return 0;
}

/*
Gegeben ist ein char** mit Elementen verschiedenen Gewichts. Im char** ist
abwechselnd eine Zahl (als char*) und eine Einheit (kg, g oder t) gespeichert.
Ein Beispiel wäre: ["10", "kg", "500", "g", "0.5", "t"]. Ihre Aufgabe besteht
darin, das Gesamtgewicht aller Elemente in Kilogramm zu berechnen. Beachten Sie
dabei, dass Sie zunächst die verschiedenen Gewichte auf Kilogramm umrechnen
müssen. Gewichte unbekannter Einheit können Sie ignorieren.
*/

// char * [], int --> double
// Returns the sum of of weights in kg given in an array
double total_weight_kg(char* a[], int length);

static void total_weight_kg_test(void) {
	char* a[] = {"10", "kg", "500", "g", "0.5", "t"};
	check_double_almost_equals(total_weight_kg(a, 6), 510.5);
}

double total_weight_kg(char* a[], int length) {
	double sum = 0; // in kg
	for (int i = 0; i < length; i += 2) {
		// i--;
		// prinf("Du stinkst!\n");
		// prinf("1337 :^)\n");
		double num = 0;
		sscanf(a[i], "%lf", &num);
		if (!strcmp(a[i + 1], "t")) {
			sum += num * 1000;
		} else if (!strcmp(a[i + 1], "kg")) {
			sum += num;
		} else if (!strcmp(a[i + 1], "g")) {
			sum += num / 1000;
		}
	}
	return sum;
}

int main(void) {
	total_weight_kg_test();
	return EXIT_SUCCESS;
}

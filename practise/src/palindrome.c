/*
Compile: make palindrome
Run: ./palindrome
make palindrome && ./palindrome
*/

#include "base.h"
#include "string.h"

/**
(a) Todo: Implement.
Return whether a character is in the alphabet
*/
int is_in_alphabet(char c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

/**
(b) Todo: Implement.
Return whether a String is a palindrome
*/
int is_palindrome(char* s) {
    int len = strlen(s);

    int x = 0;
    int y = len - 1;

    while(x < y) {
        if (!is_in_alphabet(s[x])) x++;
        else if (!is_in_alphabet(s[y])) y--;
        else if (s[x] == s[y]) {
            x++;
            y--;
        } else return 0;
    }

    return len > 0;
}

/**
(c) Todo: Implement.
Return whether a String contains at least one palindrome of minimum size minimumPalindromeSize
*/
int contains_palindrome(char* s, int minimumPalindromeSize) {
    int len = strlen(s);

    for(int i = 0; i < len - minimumPalindromeSize; i++) {
        for(int j = i + minimumPalindromeSize; j <= len; j++) {
            if (is_palindrome(s_sub(s, i, j))) {
                return 1;
            }
        }
    }

    return 0;
}




void test(void) {
    // (a)
    check_expect_i(is_in_alphabet('a'), 1);
    check_expect_i(is_in_alphabet('y'), 1);
    check_expect_i(is_in_alphabet('B'), 1);
    check_expect_i(is_in_alphabet('X'), 1);
    check_expect_i(is_in_alphabet(' '), 0);
    check_expect_i(is_in_alphabet('.'), 0);
    check_expect_i(is_in_alphabet('{'), 0);

    // (b)
    check_expect_i(is_palindrome("hello world"), 0);
    check_expect_i(is_palindrome("anna"), 1);
    check_expect_i(is_palindrome(""), 0);
    check_expect_i(is_palindrome("shower."), 0);
    check_expect_i(is_palindrome("madam ?"), 1);
    check_expect_i(is_palindrome("nurses run"), 1);

    // (c)
    check_expect_i(contains_palindrome("hello world", 5), 0);
    check_expect_i(contains_palindrome("hello world", 3), 1);
    check_expect_i(contains_palindrome("anna", 3), 1);
    check_expect_i(contains_palindrome("", 0), 0);
    check_expect_i(contains_palindrome("shower thoughts by madam anna", 4), 1);
    check_expect_i(contains_palindrome("madam anna is a nurse", 3), 1);
    check_expect_i(contains_palindrome("nurses run", 4), 1);

}

int main(void) {
    test();
    return 0;
}

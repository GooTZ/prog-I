/*
Compile: make sequence_count
Run: ./sequence_count
make sequence_count && ./sequence_count
*/

#include "base.h"
#include "string.h"

/**
(a) Todo: Implement.
Returns number of positions at which t occurs in s.
*/
int sequence_count(String s, String t) {
    int count = 0;
    int last_index = -1;
    while ((last_index = s_index_from(s, t, last_index + 1)) >= 0) {
        count++;
    }
    return count;
}

/**
(b) Todo: Fix the bug.
Returns true if (and only if) the parentheses in s match and form a
correctly parenthesized expression. The function just checks the
parentheses and ignores any other characters.
*/
bool parentheses_correct(String s) {
    int n = s_length(s);
    int p = 0;
    for (int i = 0; i < n; i++) {
        if (s[i] == '(') p++;
        if (s[i] == ')') p--;

        if (p < 0) return false;
    }
    return p == 0;
}

void test(void) {
    // (a)
    check_expect_i(sequence_count("hello world", "l"), 3);
    check_expect_i(sequence_count("hello world", "w"), 1);
    check_expect_i(sequence_count("hello worlld", "ll"), 2);
    check_expect_i(sequence_count("hello world  ", " "), 3);
    check_expect_i(sequence_count("hello world hello", "hello"), 2);
    check_expect_i(sequence_count("hello world", "not"), 0);
    check_expect_i(sequence_count("hello world", "not in there..."), 0);
    check_expect_i(sequence_count("...", "..."), 1);
    check_expect_i(sequence_count("....", "..."), 2);
    check_expect_i(sequence_count(".....", "..."), 3);

    // (b)
    check_expect_b(parentheses_correct("(3"), false);
    check_expect_b(parentheses_correct("3)"), false);
    check_expect_b(parentheses_correct(")3("), false);
    check_expect_b(parentheses_correct("(3)"), true);
    check_expect_b(parentheses_correct("((3))"), true);
    check_expect_b(parentheses_correct("((3)"), false);
    check_expect_b(parentheses_correct("((3)))"), false);
    check_expect_b(parentheses_correct("()((3))"), true);
    check_expect_b(parentheses_correct("(1)+(2)"), true);
    check_expect_b(parentheses_correct(""), true);
}

int main(void) {
    test();
    return 0;
}

// compile & run: javac HappyNumbers.java && java HappyNumbers
import java.util.*;

public class HappyNumbers {
    List<Integer> hnList = new ArrayList<Integer>();

    /**
    Returns a list of Happy Numbers
    */
    public List<Integer> calculateList(int upperLimit) {
        hnList.clear();

        hnList.add(0);
        for(int i = 1; i < upperLimit; i+=2) {
            hnList.add(i);
        }

        int pos = 2;
        while(pos < hnList.size()) {
            int remove = hnList.get(pos);
            for(int i = 1; i * remove - i + 1 < hnList.size(); i++) {
                hnList.remove(i * remove - i + 1);
            }
            pos++;
        }

        return hnList;
    }

    public void printHappyNumbers(int count) {

        List<Integer> hns = calculateList(count);

        System.out.println("\nExpected result:\n1 3 7 9 13 15 21 25 31 33 37 43 49 51 63 67 69 73 75 79 87 93 99");

        System.out.println("\nActual result:");
        for (int i = 1; i < hns.size(); i++) {
            System.out.print(hns.get(i) + " ");
        }
        System.out.println();
    }

    public static void main(String... args) {
        HappyNumbers hn = new HappyNumbers();

        hn.printHappyNumbers(100);


    }
}

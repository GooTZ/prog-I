/*
Linux / MacOS:
Compile:
javac -cp .:prog1javalib.jar Set.java
Run:
java -cp .:prog1javalib.jar Set
Compile and run:
javac -cp .:prog1javalib.jar Set.java && java -cp .:prog1javalib.jar Set

WINDOWS: Replace all : with ;

*/

import prog1.base.Base;
import java.util.StringJoiner;

public class Set {
    private class Item {
        public String value;
        public Item next;
        public Item(String value, Item next) {
            this.value = value;
            this.next = next;
        }
    }

    public class Iterator {
        private int curBucket = -1;
        private Item curItem = null;

        private Iterator() {}

        public boolean hasNext() {
            if(curItem != null && curItem.next != null) {
                return true;
            }

            for(int bucketNr = curBucket + 1; bucketNr < N; ++bucketNr) {
                if(buckets[bucketNr] != null) {
                    return true;
                }
            }
            return false;
        }

        public String next() {
            if(curItem != null && curItem.next != null) {
                curItem = curItem.next;
                return curItem.value;
            }

            for(int bucketNr = curBucket + 1; bucketNr < N; ++bucketNr) {
                if(buckets[bucketNr] != null) {
                    curBucket = bucketNr;
                    curItem = buckets[bucketNr];
                    return curItem.value;
                }
            }

            return null;
        }
    }

    private final int N;
    private Item[] buckets;
    private int n = 0;

    public Set() {
        this(4); // calls the other constructor
    }

    private Set(int bucketCount) {
        N = bucketCount;
        buckets = new Item[bucketCount];
    }

    private int hash(String value) {
        int hash = 0;
        for(char c : value.toCharArray()) {
            hash = hash * 31 + c;
        }
        return Math.abs(hash);
    }

    // add String to the end of this set
    public void add(String s) {
        if(this.contains(s)) {
            return;
        }

        int bucketNr = hash(s) % N;
        if(buckets[bucketNr] == null) {
            buckets[bucketNr] = new Item(s, null);
        } else {
            Item i = buckets[bucketNr];
            while(i.next != null) {
                i = i.next;
            }
            i.next = new Item(s, null);
        }

        n++;
    }

    public boolean contains(String s) {
        int bucketNr = hash(s) % N;
        for(Item i = buckets[bucketNr]; i != null; i = i.next) {
            if(i.value.equals(s)) {
                return true;
            }
        }

        return false;
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    public Iterator getIterator() {
        return new Iterator();
    }

    public String toString() {
        StringJoiner sj = new StringJoiner(", ", "Set(", ")");

        Iterator i = getIterator();
        while(i.hasNext()) {
            sj.add(i.next());
        }

        return sj.toString();
    }

    // return a new Set, containing only Strings which end with the designated letter
    public Set endsWithFilter(String endingString) {
        Set result = new Set();

        Iterator i = getIterator();
        while(i.hasNext()) {
            String curr = i.next();
            if(curr.endsWith(endingString)) {
                result.add(curr);
            }
        }

        return result;
    }


    // return the element which has the maximum average character value
    public String maximumAverageCharacterValue() {
        String result = "";

        Iterator i = getIterator();
        while(i.hasNext()) {
            String curr = i.next();

            int lastSum = 0;
            for(char c : result.toCharArray()) {
                lastSum += (int) c;
            }

            int currSum = 0;
            for(char c : curr.toCharArray()) {
                currSum += (int) c;
            }

            if(!curr.isEmpty() && (result.isEmpty() || (lastSum / result.length() < currSum / curr.length()))){
                result = curr;
            }
        }

        return result;
    }




    public static void main(String[] args) {
        Set set = new Set();

        String[] testWordsA = {"there", "which", "picture", "their", "other", "small", "large"};
        String[] testWordsB = {"there", "which", "picture", "change", "spell", "animal", "house"};

        for(String s : testWordsA) {
            set.add(s);
        }

        Set otherSet = new Set();
        for(String s : testWordsB) {
            otherSet.add(s);
        }

        System.out.println("\nSet A: " + set);
        System.out.println("Set B: " + otherSet + "\n");

        Base.checkExpect(set.toString(), "Set(there, other, which, picture, their, small, large)");
        Base.checkExpect(otherSet.toString(), "Set(there, change, spell, animal, house, which, picture)");

        Base.checkExpect(set.size(), 7);
        Base.checkExpect(otherSet.size(), 7);
        Base.checkExpect(set.contains("their"), true);
        Base.checkExpect(set.contains("hello world"), false);
        Base.checkExpect(set.maximumAverageCharacterValue(), "other");
        Base.checkExpect(otherSet.maximumAverageCharacterValue(), "house");

        Set endsWithR = set.endsWithFilter("r");

        Base.checkExpect(endsWithR.contains("their"), true);
        Base.checkExpect(endsWithR.contains("picture"), false);



    }
}

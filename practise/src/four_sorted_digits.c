/*
Compile: make four_sorted_digits
Run: ./four_sorted_digits
Compile and run:
make four_sorted_digits && ./four_sorted_digits
*/

#include "base.h"
#include "string.h"

/*
Returns true if s contains at least 4 incrementing digits right behind one another. Else it returns false.
*/
bool four_sorted_digits(String s) {
    int i = 0;
    char last;
    char curr;
    while((curr = *s)) {
        if(curr >= '0' && curr <= '9') {
            if(i == 0 || last <= curr) {
                i++;
                if(i >= 4) {
                    return true;
                }
            }
            last = curr;
        } else {
            i = 0;
        }
        s++;
    }
    return false;
}

void four_sorted_digits_test(void) {
    check_expect_b(four_sorted_digits(""), false);
    check_expect_b(four_sorted_digits("123"), false);
    check_expect_b(four_sorted_digits("abcd"), false);
    check_expect_b(four_sorted_digits("1234"), true);
    check_expect_b(four_sorted_digits("2479"), true);
    check_expect_b(four_sorted_digits("1111"), true);
    check_expect_b(four_sorted_digits("a123"), false);
    check_expect_b(four_sorted_digits("123a"), false);
    check_expect_b(four_sorted_digits("1234a"), true);
    check_expect_b(four_sorted_digits("xx1234y"), true);
    check_expect_b(four_sorted_digits("abc12345"), true);
    check_expect_b(four_sorted_digits("a 1 2 3 4 c"), false);
    check_expect_b(four_sorted_digits("a 3333 c"), true);
}

int main(void) {
    base_init();
    base_set_memory_check(true);
    four_sorted_digits_test();
    return 0;
}

/*
Linux / MacOS:
Compile:
javac -cp .:prog1javalib.jar TableOfContentsTree.java
Run:
java -cp .:prog1javalib.jar TableOfContentsTree
Compile and run:
javac -cp .:prog1javalib.jar TableOfContentsTree.java && java -cp .:prog1javalib.jar TableOfContentsTree

WINDOWS: Replace all : with ;

*/

import prog1.base.Base;

class Node {
    public String title;
    public Node[] subsections;

    public Node(String title, Node... subsections) {
        this.title = title;
        this.subsections = subsections;
    }

    /** (b) Returns a string representation of the table-of-contents tree.
        See the expected output below. */
    public String tableOfContents(String id) {
        String out = id + " " + title + "\n";
        for (int i = 1; i <= subsections.length; i++) {
            out += subsections[i - 1].tableOfContents(id + i + ".");
        }
        return out;
    }

}

class Tree {

    private Node root = null;

    public Tree() { }

    public Tree(Node root) {
        this.root = root;
    }

    /** (a) Returns a string representation of the table-of-contents tree.
        See the expected output below. Handle the empty tree here. If the
        tree is not empty, call the corresponding Node method. */
    public String tableOfContents() {
        return root == null ? "" : root.tableOfContents("");
    }

}

public class TableOfContentsTree {

    public static Node s(String section, Node... subsections) {
        return new Node(section, subsections);
    }

    public static void test() {
        Tree t = new Tree(s("My Thesis",
            s("Introduction", s("Motivation"), s("Contributions")),
            s("Background", s("Related Work on Foo"), s("Related Work on Bar"), s("Summary")),
            s("Implementation", s("Architecture"), s("Main Classes", s("Class 1"), s("Class 2"))),
            s("Conclusion", s("Limitations", s("Future Work")))));
        // Base.println("|" + t.tableOfContents() + "|");

        Base.checkExpect(t.tableOfContents(),
            " My Thesis\n" +
            "1. Introduction\n" +
            "1.1. Motivation\n" +
            "1.2. Contributions\n" +
            "2. Background\n" +
            "2.1. Related Work on Foo\n" +
            "2.2. Related Work on Bar\n" +
            "2.3. Summary\n" +
            "3. Implementation\n" +
            "3.1. Architecture\n" +
            "3.2. Main Classes\n" +
            "3.2.1. Class 1\n" +
            "3.2.2. Class 2\n" +
            "4. Conclusion\n" +
            "4.1. Limitations\n" +
            "4.1.1. Future Work\n");
        Base.summary();
    }

    public static void main(String[] args) {
        test();
    }

}

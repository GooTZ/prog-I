/*
Linux / MacOS:
Compile:
javac -cp .:prog1javalib.jar DirectoryTree.java
Run:
java -cp .:prog1javalib.jar DirectoryTree
Compile and run:
javac -cp .:prog1javalib.jar DirectoryTree.java && java -cp .:prog1javalib.jar DirectoryTree

WINDOWS: Replace all : with ;

*/

import prog1.base.Base;

class Node {
    public String directory;
    public Node[] subdirectories;

    public Node(String directory, Node... subdirectories) {
        this.directory = directory;
        this.subdirectories = subdirectories;
    }

    /** (b) Returns a string representation of the directory tree.
        See the expected output below. */
    public String directory(String path) {
        String result = path + "/" + directory + "\n";

        for (Node dir : subdirectories) {
            result += dir.directory(path + "/" + directory);
        }

        return result;
    }

}

class Tree {

    private Node root = null;

    public Tree() { }

    public Tree(Node root) {
        this.root = root;
    }

    /** (a) Returns a string representation of the directory tree. See the
        expected output below. Handle the empty tree here. (The empty tree
        should be represented by the empty string.) If the tree is not
        empty, call the corresponding Node method. */
    public String directory() {
        return root == null ? "" : root.directory("");
    }

}

public class DirectoryTree {

    public static Node s(String section, Node... subdirectories) {
        return new Node(section, subdirectories);
    }

    public static void test() {
        Tree t = new Tree(s("home",
            s("games", s("game1"), s("game2")),
            s("C", s("Assignment1"), s("prog1lib", s("lib"), s("doc"))),
            s("Java", s("Assignment1", s("Exercise1")), s("Assignment2", s("Exercise1"), s("Exercise2")))));
        // Base.println(t.directory());
        Base.checkExpect(t.directory(),
            "/home\n" +
            "/home/games\n" +
            "/home/games/game1\n" +
            "/home/games/game2\n" +
            "/home/C\n" +
            "/home/C/Assignment1\n" +
            "/home/C/prog1lib\n" +
            "/home/C/prog1lib/lib\n" +
            "/home/C/prog1lib/doc\n" +
            "/home/Java\n" +
            "/home/Java/Assignment1\n" +
            "/home/Java/Assignment1/Exercise1\n" +
            "/home/Java/Assignment2\n" +
            "/home/Java/Assignment2/Exercise1\n" +
            "/home/Java/Assignment2/Exercise2\n");
        Base.summary();
    }

    public static void main(String[] args) {
        test();
    }

}

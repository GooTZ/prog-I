# About
In dieser Repository befinden sich die Hausaufgaben von Lars Wrenger und Domenik Weber der Programmieren I Vorlesung der Leibniz Universität Hannover.

# Build guide
## Eine bestimmte Übung builden
Das Makefile im root-Verzeichnis des Repo bietet für jeden Unterordner eine Buildmöglichkeit, so z.B.
```
make 10-21-16
```
Damit werden alle für die Abgabe relevaten Dinge dem build-Prozess unterzogen und in ein Archiv ```solution.zip``` im entsprechenden Verzeichnis gepackt.

## Teile einer Übung builden
Innerhalb jedes Übungs-directory besteht die Möglichkeit einzelne Teile zu builden. Mögliche Build-targets sind:

```pdf```: Aus solution.tex wird eine solution.pdf generiert.

```pdf_clean```: Die Überreste des LaTeX build-Prozesses werden entfernt.

```code```: Erstellt aus dem Inhalt des ```/src``` Verzeichnisses die Ausführbare ```./code```.

```zip```: Erstellt ein für die Abgabe bereites .zip Archiv mit allen nötigen Dateien.

# Sonstiges

Besser wäre ein Wechsel von ```pdflatex``` zu ```latex```, das noch in BasicTex
enthalten ist und fast die gleichen Features beinhaltet.

    latex -interaction=nonstopmode -output-format=pdf solution.tex

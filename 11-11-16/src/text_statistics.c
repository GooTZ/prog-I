/*
   Compile: make text_statistics
   Run: ./text_statistics
 */

#include "base.h"
#include "string.h"

/*
   Entwickeln Sie eine Funktion zur Erstellung einer Text-Statistik.
   Eingabe der Funktion ist ein String beliebiger Länge. Ausgabe ist eine
   statistische Zusammenfassung des Strings, die folgende Komponenten enthält:
   Anzahl Zeichen insgesamt (inkl. Whitespace), Anzahl Buchstaben, Anzahl Wörter,
   Anzahl Sätze, Anzahl Zeilen. Whitespace (Leerraum) ist definiert als die
   Menge der Steuerzeichen Leerzeichen (' '), Tabulator ('\t'), Zeilenvorschub
   ('\n') und Wagenrücklauf ('\r'). Die Menge der im Eingabetext erlaubten
   Zeichen sind diejenigen mit einem ASCII-Code zwischen 32 (Leerzeichen) und
   126 (Tilde) sowie Whitespace (also insbesondere sind keine Umlaute erlaubt).
 */

typedef struct Statistic {
    int chars;
    int letters;
    int words;
    int sentences;
    int lines;
} Statistic;

// Constructor
Statistic make_statistic(int chars, int letters, int words, int sentences, int lines) {
    Statistic s = { chars, letters, words, sentences, lines };
    return s;
}

typedef enum CharType {
    LETTER,
    DIGIT,
    SENTENCE_TERMINATOR,
    WHITESPACE,
    LINEBREAK,
    OTHER
} CharType;

// Return the character class of the given char
CharType char_type(char c) {
    if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
        return LETTER;
    }
    if(c >= '0' && c <= '9') {
        return DIGIT;
    }
    if(c == '.' || c == '!' || c == '?') {
        return SENTENCE_TERMINATOR;
    }
    if(c == ' ' || c == '\t' || c == '\r') {
        return WHITESPACE;
    }
    if(c == '\n') {
        return LINEBREAK;
    }
    return OTHER;
}

// Creates a text-statistic to the given string
Statistic text_statistics(String s) {
    Statistic stats = make_statistic(s_length(s), 0, 0, 0, 0);

    if(stats.chars > 0) {
        stats.lines = 1;
    }

    CharType last_char_type = OTHER;

    for(int i = 0; i < stats.chars; i++) {
        CharType curr_char_type = char_type(s[i]);

        switch(curr_char_type) {
        case LETTER:
            stats.letters++;
            if(last_char_type != LETTER) {
                // counts words on their beginnig
                stats.words++;
            }
            break;

        case SENTENCE_TERMINATOR:
            if(last_char_type == LETTER || last_char_type == DIGIT) {
                // Each sentences has to end with a point
                stats.sentences++;
            }
            break;

        case DIGIT:
            break;

        case WHITESPACE:
            break;

        case LINEBREAK:
            stats.lines++;
            break;

        case OTHER:
            break;
        }
        last_char_type = curr_char_type;
    }
    return stats;
}

// check function for statistics
static int check_expect_statistics(int line, Statistic s1, Statistic s2) {
    printf("checks start:\n");
    return base_check_expect_i(__FILE__, line, s1.chars, s2.chars)
           && base_check_expect_i(__FILE__, line, s1.letters, s2.letters)
           && base_check_expect_i(__FILE__, line, s1.words, s2.words)
           && base_check_expect_i(__FILE__, line, s1.sentences, s2.sentences)
           && base_check_expect_i(__FILE__, line, s1.lines, s2.lines);
}

static void text_statistics_test() {
    check_expect_statistics(__LINE__,
            text_statistics("#978-3462046328%%"),
            make_statistic(17, 0, 0, 0, 1));
    check_expect_statistics(__LINE__,
            text_statistics("Lalala. Ich back mir nen kakao."),
            make_statistic(31, 24, 6, 2, 1));
    check_expect_statistics(__LINE__,
            text_statistics("Duis autem vel eum iriure dolor in hendrerit in\n vulputate velit esse molestie consequat, \nvel illum dolore eu feugiat nulla facilisis. "),
            make_statistic(136, 111, 21, 1, 3));
    check_expect_statistics(__LINE__,
            text_statistics("#123\n test test...\n\r\tnumber 4?!?\n i hope  it works."),
            make_statistic(51, 26, 7, 3, 4));
}

int main(void) {
    text_statistics_test();
    return EXIT_SUCCESS;
}

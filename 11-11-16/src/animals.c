/*
Compile: make animals
Run: ./animals
*/

#include "base.h"
#include "string.h"

/*
In einem Programm sollen verschiedene Formen von geometrischen Körpern, nämlich Zylinder, Kugel und Quader, repräsentiert werden. Entwickeln Sie eine Funktion, die diese geometrischen Körper vararbeiten kann (Parametertyp: Shape) und das zugehörige Volumen berechnet.
*/

enum animal_tag {
	ANIMAL_CAT,
	ANIMAL_DOG,
	ANIMAL_SHARK
};

struct animal_cat {
	bool likes_wool;
};

struct animal_dog {
	double length;
};

struct animal_shark {
	int surfer_count;
	bool hungry;
};

struct animal {
	enum animal_tag tag;
	String name;
	double weight;
	union {
		struct animal_cat cat;
		struct animal_dog dog;
		struct animal_shark shark;
	};
};

// constructor for cat
struct animal make_animal_cat(String name, double weight, bool likes_wool) {
	struct animal a;
	a.tag = ANIMAL_CAT;
	a.name = name;
	a.weight = weight;
	a.cat.likes_wool = likes_wool;
	return a;
}

// constructor for dog
struct animal make_animal_dog(String name, double weight, double length) {
	struct animal a;
	a.tag = ANIMAL_DOG;
	a.name = name;
	a.weight = weight;
	a.dog.length = length;
	return a;
}

// consutructor for shark
struct animal make_animal_shark(String name, double weight, int surfer_count, bool hungry) {
	struct animal a;
	a.tag = ANIMAL_SHARK;
	a.name = name;
	a.weight = weight;
	a.shark.surfer_count = surfer_count;
	a.shark.hungry = hungry;
	return a;
}

// struct animal -> String
// Creates a text from an animal struct
void make_text_from_animal(struct animal a);

static void animal_test() {
	make_text_from_animal(make_animal_cat("Hans", 10, false));
	make_text_from_animal(make_animal_cat("Joachim", 9.5, true));
	make_text_from_animal(make_animal_cat("Herman", 19, false));

	make_text_from_animal(make_animal_dog("Werner", 20, 15.7));
	make_text_from_animal(make_animal_dog("Rudi", 14, 22));
	make_text_from_animal(make_animal_dog("Rudi", 17, 17));

	make_text_from_animal(make_animal_shark("Ernst", 100, 33, false));
	make_text_from_animal(make_animal_shark("Reiner", 100, 53, true));
	make_text_from_animal(make_animal_shark("Hunter", 100, 93, true));
}

// Computes the distance from the given point
// to the origin of the coordinate system.
void make_text_from_animal(struct animal a) {
	switch(a.tag) {
		case ANIMAL_DOG:
		printf("I am %s the dog, I weight %.2f and my collar is %.2f cm long!\n", a.name, a.weight, a.dog.length);
		break;

		case ANIMAL_CAT:
		printf("I am %s the cat, I weight %.2f kg and ", a.name, a.weight);
		if(a.cat.likes_wool) {
			printf("I love wool!\n");
		} else {
			printf("I hate wool!\n");
		}
		break;

		case ANIMAL_SHARK:
		printf("I am %s the shark, I weight %.2f and I am ", a.name, a.weight);
		if(a.shark.hungry) {
			printf("very hungry, although I already ate %i surfers!\n", a.shark.surfer_count);
		} else {
			printf("not hungry because I already ate %i surfers\n", a.shark.surfer_count);
		}
		break;
	}

	return;
}

int main(void) {
	animal_test();
	return EXIT_SUCCESS;
}
